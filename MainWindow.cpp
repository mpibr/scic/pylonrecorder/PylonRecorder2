#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(Capture* capture, CommandLineOptions options, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QList<int> sizes;
    sizes << 500 << ui->horizontalLayout_right->sizeHint().height();
    ui->splitter_control->setSizes(sizes);

    this->setGeometry(options.wndX, options.wndY, options.wndW, options.wndH);

    QString frameRateString;
    if (capture->frameRate >0 ){
        frameRateString = QString::number(capture->frameRate);
    }else{
        frameRateString = "dynamic ";
    }

    cameraInfo = (capture->modelName + " (" + capture->serialNumber + ")" + " @"
                  + frameRateString + "fps"
                  + " " + QString::number(capture->frameSize.width)
                  + "x" + QString::number(capture->frameSize.height));

    ui->statusBar->showMessage(cameraInfo);
    ui->statusBar->setToolTip("Current settings of the camera and status of acquisition");
    label_recorded = new QLabel(this);
    label_acquired = new QLabel(this);
    label_statusSeparator = new QLabel(this);
    label_recorded->setText("0");
    label_recorded->setToolTip("Frames written to disc");
    label_acquired->setText("0");
    label_acquired->setToolTip("Frames acquired");
    label_statusSeparator->setText(" / ");

    label_RAMDescription = new QLabel(this);
    label_availableRAM = new QLabel(this);
    label_RAMSeparator = new QLabel(this);
    label_totalRAM = new QLabel(this);
    label_RAMDescription->setText("RAM:");
    label_availableRAM->setToolTip("Available RAM");
    label_RAMSeparator->setText("/");
    label_totalRAM->setToolTip("Total RAM");
    label_totalRAM->setMinimumWidth(80);

    label_calculatedFrameRateDescription = new QLabel(this);
    label_calculatedFrameRate = new QLabel(this);
    label_calculatedFrameRateDescription->setText("Fps:");
    label_calculatedFrameRate->setToolTip("Calculated framerate");
    label_calculatedFrameRate->setMinimumWidth(80);

    ui->statusBar->addPermanentWidget(label_calculatedFrameRateDescription);
    ui->statusBar->addPermanentWidget(label_calculatedFrameRate);

    ui->statusBar->addPermanentWidget(label_RAMDescription);
    ui->statusBar->addPermanentWidget(label_availableRAM);
    ui->statusBar->addPermanentWidget(label_RAMSeparator);
    ui->statusBar->addPermanentWidget(label_totalRAM);

    ui->statusBar->addPermanentWidget(label_recorded);
    ui->statusBar->addPermanentWidget(label_statusSeparator);
    ui->statusBar->addPermanentWidget(label_acquired);

    scene = new QGraphicsScene(this);
    pixmapItem = new QGraphicsPixmapItem();
    scene->addItem(pixmapItem);

    ui->checkBox_trackingPreview->setVisible(false);
    ui->checkBox_recordTracking->setVisible(false);
    ui->label_trackingFileName->setVisible(false);
    ui->lineEdit_trackingFileName->setVisible(false);
    ui->pushButton_selectTrackingFile->setVisible(false);

    guiOptions.filePath = options.defaultPath;

    cameraEngine = new CameraEngine(capture, options, ui->verticalLayout_Plugin, scene, this);

    // Initialize others
    ramMonitorThread = new QThread(this);
    ramMonitor = new RamMonitor();
    label_totalRAM->setText(QString::number(ramMonitor->totalRAM));

    // About Dialog
    QAction* actionAbout = new QAction("&About",this);
    ui->mainToolBar->addAction(actionAbout);

    // Validators
    QIntValidator *intValidator = new QIntValidator(this);
    intValidator->setBottom(0); //maximum defaults to int max
    QRegExp longlongRegEx("[0-9]*");
    QRegExp aviFilenameRegEx("*.avi");
    aviFilenameRegEx.setPatternSyntax(QRegExp::Wildcard);
    QValidator* longlongValidator = new QRegExpValidator(longlongRegEx, this);
    QValidator* aviFileNameValidator = new QRegExpValidator(aviFilenameRegEx, this);
    ui->lineEdit_nFrames->setValidator(longlongValidator);
    ui->lineEdit_fileName->setValidator(aviFileNameValidator);
    ui->lineEdit_nFrames->setValidator(intValidator);

    connect(ui->pushButton_softwareTrigger, &QPushButton::clicked, cameraEngine, &CameraEngine::start);
    connect(ui->pushButton_stop, &QPushButton::clicked, cameraEngine, &CameraEngine::stop);
    connect(this, &MainWindow::guiOptionsChanged, cameraEngine, &CameraEngine::onGuiOptionsChanged);

    connect(cameraEngine, &CameraEngine::newAutoFileName, [this](const QString fileName, const QString trackingfileName) {
        ui->lineEdit_fileName->setText(fileName);
        ui->lineEdit_trackingFileName->setText(trackingfileName);
    });

    connect(cameraEngine, &CameraEngine::statusInfo, [this](QString info){
        ui->statusBar->showMessage(cameraInfo + " Last action: " + info);
    });

    connect(cameraEngine, &CameraEngine::previewImage, [this](QImage image){
        pixmapItem->setPixmap(QPixmap::fromImage(image));
    });

    connect(cameraEngine, &CameraEngine::calculatedFrameRate, [this](const double &frameRate) {
        label_calculatedFrameRate->setText(QString::number(frameRate, 'f', 2));
    });

    connect(cameraEngine, &CameraEngine::frameIndexChanged, [this](const long long &index) {
        label_acquired->setText(QString::number(index));
    });

    connect(cameraEngine, &CameraEngine::writtenFrameIndexChanged, [this](const long long &index) {
        label_recorded->setText(QString::number(index));
    });

    connect(cameraEngine, &CameraEngine::lineInputEvent, ui->checkBox_record, &QCheckBox::setChecked);

    connect(cameraEngine, &CameraEngine::pluginLoaded, [this](bool isLogFileRecording){
            ui->checkBox_trackingPreview->setVisible(true);
            // Temporarily disabled since most application store this data through socket
            ui->checkBox_recordTracking->setVisible(isLogFileRecording);
            ui->label_trackingFileName->setVisible(isLogFileRecording);
            ui->lineEdit_trackingFileName->setVisible(isLogFileRecording);
            ui->pushButton_selectTrackingFile->setVisible(isLogFileRecording);
        });

    connect(cameraEngine, &CameraEngine::socketQuit, this, &MainWindow::close);
    connect(this, &MainWindow::guiClosing, cameraEngine, &CameraEngine::guiClosing);

    connect(ui->checkBox_trackingPreview, &QCheckBox::clicked, cameraEngine, &CameraEngine::onTrackingPreviewChanged);

    // Connection Main - About
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(onAbout()));

    // Connection Main - RamMonitor
    connect(ramMonitor, &RamMonitor::availableRAM, [this](const long &newValue) {
            label_availableRAM->setText(QString::number(newValue));
        } );

    // Move Ram Monitor to separate Thread
    ramMonitor->moveToThread(ramMonitorThread);
    connect(ramMonitorThread, SIGNAL(finished()), ramMonitor, SLOT(deleteLater()));
    ramMonitorThread->start();

    // Set more default GUI settings based on command line options
    if(options.autoFileName){
        ui->checkBox_autoFileName->setChecked(true);
        this->on_checkBox_autoFileName_clicked(true);
    }
    if(options.record){
        ui->checkBox_record->setChecked(true);
        this->on_checkBox_record_clicked(true);
    }
    if(options.isTriggered){
        ui->checkBox_externalTrigger->setChecked(true);
        this->on_checkBox_externalTrigger_clicked(true);
    }
    if(options.framesToCapture > 0){
        ui->lineEdit_nFrames->setText(QString::number(options.framesToCapture));
    }

    switch (options.captureMode){
    case CAPTURE_CONTINUOUS:
        ui->radioButton_continuous->setChecked(true);
        this->on_radioButton_continuous_clicked();
        break;
    case CAPTURE_BURST:
        ui->radioButton_burst->setChecked(true);
        this->on_radioButton_burst_clicked();
        break;
    case CAPTURE_FRAMEWISE:
        ui->radioButton_frameWise->setChecked(true);
        this->on_radioButton_frameWise_clicked();
        break;
    }

    QPixmap blackImage(capture->frameSize.width, capture->frameSize.height);
    blackImage.fill(Qt::black);
    pixmapItem->setPixmap(blackImage);
    ui->graphicsView_frame->setScene(scene);
    ui->graphicsView_frame->setViewport(new QOpenGLWidget);

    // Somehow forces the sceneRect to update
    ui->graphicsView_frame->setSceneRect(ui->graphicsView_frame->sceneRect());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    (void)event; //do nothing
    emit(guiClosing());
}

MainWindow::~MainWindow()
{
    if(ramMonitorThread){
        ramMonitorThread->quit();
        ramMonitorThread->wait();
    }

    // delete recordingWorker;
    delete pixmapItem;
    delete ui;
}

// Automatically connected UI Slots
void MainWindow::on_lineEdit_nFrames_editingFinished()
{
    // Set the number of frames that should be recorded/captured
    if(ui->lineEdit_nFrames->text().isEmpty()){
        guiOptions.nImagesToGrab = 0;
    }else{
        guiOptions.nImagesToGrab = ui->lineEdit_nFrames->text().toInt();
    }
    int burstTime = int(1000*(guiOptions.nImagesToGrab) / cameraEngine->getCameraFrameRate()); //Burst time in seconds
    ui->label_burstTime->setText(" = " + TimeConversion::stringConvert(burstTime) + " [hh:mm:ss:ms]");

    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_radioButton_continuous_clicked()
{
    ui->lineEdit_nFrames->setEnabled(false);
    ui->label_burstTime->setText("");

    guiOptions.captureMode = CAPTURE_CONTINUOUS;
    guiOptions.nImagesToGrab = 0;
    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_radioButton_frameWise_clicked()
{
    ui->lineEdit_nFrames->setEnabled(false);
    ui->label_burstTime->setText("");

    guiOptions.captureMode = CAPTURE_FRAMEWISE;
    guiOptions.nImagesToGrab = 0;
    emit(guiOptionsChanged(guiOptions));
}


void MainWindow::on_radioButton_burst_clicked()
{
    uint nImagesToGrab;
    ui->lineEdit_nFrames->setEnabled(true);
    // Set the number of frames that should be recorded/captured
    if(ui->lineEdit_nFrames->text().isEmpty()){
        nImagesToGrab = 0;
    }else{
        nImagesToGrab = uint(ui->lineEdit_nFrames->text().toInt());
        int burstTime = int(1000*nImagesToGrab / cameraEngine->getCameraFrameRate()); //Burst time in seconds
        ui->label_burstTime->setText(" = " + TimeConversion::stringConvert(burstTime) + " [hh:mm:ss:ms]");
    }

    guiOptions.captureMode = CAPTURE_BURST;
    guiOptions.nImagesToGrab = nImagesToGrab;

    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_checkBox_externalTrigger_clicked(bool checked)
{
    guiOptions.hasExternalTrigger = checked;
    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_checkBox_record_clicked(bool checked)
{
    guiOptions.isRecording = checked;
    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_pushButton_selectFile_clicked()
{
    if(ui->checkBox_autoFileName->isChecked()){ // select folder only
        QString folderName = QFileDialog::getExistingDirectory(nullptr, "Folder", guiOptions.filePath);
        if(!folderName.isEmpty()){
            guiOptions.filePath = folderName;
            //the auto filename will come from the engine
        }

    }else{ // Select file and folder
        QString tmpFileName = QFileDialog::getSaveFileName(nullptr, "Save file", guiOptions.filePath,
                                                           "Video files (*.avi)");
        if(!tmpFileName.isEmpty()){
            QFileInfo fileInfo(tmpFileName);
            guiOptions.filePath = fileInfo.absolutePath();
            lineEditFileName = fileInfo.fileName();
            guiOptions.fileName = fileInfo.fileName();
            ui->lineEdit_fileName->setText(guiOptions.fileName);
        }
    }

    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_checkBox_autoFileName_clicked(bool checked)
{
    ui->lineEdit_fileName->setEnabled(!checked);
    ui->lineEdit_trackingFileName->setEnabled(!checked);

    guiOptions.fileNameAutoModeEnabled = checked;
    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_lineEdit_fileName_editingFinished()
{
    lineEditFileName = ui->lineEdit_fileName->text();
    QFileInfo fileInfo(lineEditFileName);
    lineEditFileName = fileInfo.fileName();
    guiOptions.filePath = fileInfo.absolutePath();
    guiOptions.fileName = fileInfo.fileName();
    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_checkBox_recordTracking_clicked(bool checked)
{
    guiOptions.isTrackingRecording = checked;
    emit(guiOptionsChanged(guiOptions));
}

// About dialog selected
void MainWindow::onAbout()
{
    AboutDialog* aboutDialog = new AboutDialog(this);
    aboutDialog->setAttribute(Qt::WA_DeleteOnClose);
    aboutDialog->setWindowFlags(aboutDialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    aboutDialog->show();
}

// Dynamic resizing of image based on window size
void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    ui->graphicsView_frame->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
}

// Dynamic resizing of image based on window size
void MainWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    ui->graphicsView_frame->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
}

void MainWindow::on_pushButton_selectTrackingFile_clicked()
{
    if(ui->checkBox_autoFileName->isChecked()){ //select folder only
        QString folderName = QFileDialog::getExistingDirectory(nullptr, "Folder", guiOptions.trackingFilePath);
        if(!folderName.isEmpty()){
            guiOptions.trackingFilePath = folderName;
        }

    }else{ // Select file and folder
        QString tmpFileName = QFileDialog::getSaveFileName(nullptr, "Save file", guiOptions.trackingFilePath,
                                                           "Text files (*.csv *.txt)");
        if(!tmpFileName.isEmpty()){
            QFileInfo fileInfo(tmpFileName);
            guiOptions.trackingFilePath = fileInfo.absolutePath();
            lineEditTrackingFileName = fileInfo.fileName();
            guiOptions.trackingFileName = tmpFileName;
            ui->lineEdit_trackingFileName->setText(guiOptions.trackingFileName);
        }
    }

    emit(guiOptionsChanged(guiOptions));
}

void MainWindow::on_lineEdit_trackingFileName_editingFinished()
{
    lineEditTrackingFileName = ui->lineEdit_trackingFileName->text();
    QFileInfo fileInfo(lineEditTrackingFileName);
    lineEditTrackingFileName = fileInfo.fileName();
    guiOptions.trackingFilePath = fileInfo.absolutePath();
    guiOptions.trackingFileName = guiOptions.trackingFilePath + '/' + lineEditTrackingFileName;

    emit(guiOptionsChanged(guiOptions));
}

