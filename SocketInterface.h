// Deals with (optional) socket communication with clients (e.g. Matlab)

#ifndef SOCKETINTERFACE_H
#define SOCKETINTERFACE_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QBuffer>
#include <QDataStream>
#include <QTimer>
#include <QSignalMapper>
#include "BlockReader.h"
#include "BlockWriter.h"
#include "CommandLineOptions.h"

// Parameters used as Type in outgoing packets
enum socket_protocol {
    SOCKET_TIMESTAMP = 0,
    SOCKET_START = 1,
    SOCKET_STOP = 2,
    SOCKET_FILENAME = 3,
    SOCKET_TRACKINGRESULT = 4,
    SOCKET_QUIT = 5
};

class SocketInterface: public QObject
{
    enum SocketType {SERVER, CLIENT, NONE};
    Q_OBJECT
public:
    explicit SocketInterface(QObject * parent = nullptr, QString hostname = "", unsigned int port = 0);
    ~SocketInterface();
public slots:
    //when running as server
    void onClientConnected();
    void onClientDisconnected(int clientIdx);

    //when running as client
    void onConnectServer();
    void onServerConnected();
    void onServerDisconnected();
    void onRead();

    // Sending of PylonRecorder specific signals
    void onTimeStamp(quint64 timeStamp);               // Send a socket timestamp packet
    void onStartTimeStamp(quint64 timeStamp);          // Send a socket start packet
    void onStopTimeStamp(quint64 timeStamp);           // Send a socket stop packet
    void onFileName(QString fileName);                 // Send a socket filename packet
    void onStop();                                     // Send an socket stop packet without timestamp
    void onTrackingResult(std::vector<double> result); // Send the tracking result
    void onQuit();
private:
    QSignalMapper *signalMapper;
    QTimer connectionTimer; //for client
    unsigned int connectionAttempts = 0;
    unsigned int maxNumberConnectionAttempts = 10;
    QString hostname; //only required when running as client
    unsigned int port;
    bool isConnected = false;
    QTcpServer server;
    QVector<QTcpSocket*> clients; //only one when running as client

signals:
    void statusInfo(QString info);
    void socketTimeStamp(quint64 timeStamp);
    void socketStart(quint64 timeStamp);
    void socketStop(quint64 timeStamp);
    void socketFileName(QString fileName);
    void socketTrackingResult(std::vector<double> result);
    void socketQuit(quint64 timeStamp);
};

#endif // SOCKETINTERFACE
