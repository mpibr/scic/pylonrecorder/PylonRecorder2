#include "USBCapture.h"

USBCapture::USBCapture(QObject *parent, int cameraIdx, uint stopLine, bool isTriggered, QString configFile)
    : Capture(parent, stopLine, isTriggered)
{
    modelName = "OpenCv";
    friendlyName = "OpenCv USB camera";

    cvProperties = {{"CV_CAP_PROP_FRAME_WIDTH", cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH},
                    {"CV_CAP_PROP_FRAME_HEIGHT", cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT},
                    {"CV_CAP_PROP_FPS", cv::VideoCaptureProperties::CAP_PROP_FPS},
                    {"CV_CAP_PROP_FOURCC", cv::VideoCaptureProperties::CAP_PROP_FOURCC},
                    {"CV_CAP_PROP_BRIGHTNESS", cv::VideoCaptureProperties::CAP_PROP_BRIGHTNESS},
                    {"CV_CAP_PROP_CONTRAST", cv::VideoCaptureProperties::CAP_PROP_CONTRAST},
                    {"CV_CAP_PROP_SATURATION", cv::VideoCaptureProperties::CAP_PROP_SATURATION},
                    {"CV_CAP_PROP_HUE", cv::VideoCaptureProperties::CAP_PROP_HUE},
                    {"CV_CAP_PROP_GAIN", cv::VideoCaptureProperties::CAP_PROP_GAIN},
                    {"CV_CAP_PROP_EXPOSURE", cv::VideoCaptureProperties::CAP_PROP_EXPOSURE}
                   };

    try{
        if(cameraIdx >= 0){
            cap = new cv::VideoCapture(cameraIdx); // open the default camera
        }else{
            for(cameraIdx = 0; cameraIdx<10; cameraIdx++){
                qDebug() << "Trying device " << cameraIdx;

                // Try to access first available camera
                cap = new cv::VideoCapture(cameraIdx);

                if(cap->isOpened()){
                    qDebug() << "Device " << cameraIdx << " opened";
                    break;
                }else{
                    qDebug() << "Device " << cameraIdx << " is busy";
                }
            }
        }
        if(cap->isOpened()){  // check if we succeeded
            isAvailable = true;
            //return -1;
        }else{
            std::cerr << "No compatible USB cameras found" << std::endl;
            return;
        }
    }catch (cv::Exception e){
        // Error handling.
        std::cerr << "An exception occurred. No compatible USB cameras found" << std::endl
                  << e.msg << std::endl;
        //exitCode = 1;
        return;
    }

    //Read configuration file
    if(!configFile.isEmpty()){
        QFile file(configFile);
        if(file.open(QIODevice::ReadOnly)) {
            qDebug()<<"Reading OpenCV configuration from file: " <<configFile;
            QTextStream in(&file);
            while(!in.atEnd()) {
                QString line = in.readLine();
                QStringList fields = line.split(",");
                if(fields.size()==2){
                    int value = cvProperties.value(fields.at(0));
                    if(value>0){
                        cap->set(value, fields.at(1).toDouble());
                        qDebug()<< "configuration entry:" << fields.at(0) << "," << fields.at(1);
                    }
                }
            }
            file.close();
        }
    }

    frameRate = cap->get(cv::VideoCaptureProperties::CAP_PROP_FPS);
    frameSize.width = int(cap->get(cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH));
    frameSize.height = int(cap->get(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT));
    //int channels = int(cap->get(cv::VideoCaptureProperties::CAP_PROP_CHANNEL));

    usbCaptureWorker = new USBCaptureWorker(cap);

    connect(usbCaptureWorker, SIGNAL(sendFrame(cv::Mat)), this, SIGNAL(sendFrame(cv::Mat)));
    connect(this, SIGNAL(captureNImages(long long)), usbCaptureWorker, SLOT(onCaptureNImages(long long)));
    connect(this, SIGNAL(updateFrameRate(int)), usbCaptureWorker, SLOT(onSetFrameRate(int)));
    connect(this, SIGNAL(stopGrabbing()), usbCaptureWorker, SLOT(onStopGrabbing()));

    connect(usbCaptureWorker, SIGNAL(startTimeStampEvent(quint64)), this, SIGNAL(startTimeStampEvent(quint64)));
    connect(usbCaptureWorker, SIGNAL(stopTimeStampEvent(quint64)), this, SIGNAL(stopTimeStampEvent(quint64)));

    captureThread = new QThread;
    usbCaptureWorker->moveToThread(captureThread);
    captureThread->start();
}

void USBCapture::setFrameRate(double frameRate)
{
    cap->set(cv::VideoCaptureProperties::CAP_PROP_FPS, frameRate);
    frameRate = cap->get(cv::VideoCaptureProperties::CAP_PROP_FPS);
    emit(updateFrameRate(frameRate));
}

void USBCapture::init(int captureMode, long long nImagesToCapture)
{
    emit(stopGrabbing());
    switch(captureMode){
    case CAPTURE_CONTINUOUS:
        this->nImagesToCapture =  std::numeric_limits <long long>::max();
        break;
    case CAPTURE_BURST:
        this->nImagesToCapture  = nImagesToCapture;
        break;
    case CAPTURE_FRAMEWISE:
        this->nImagesToCapture  = 1;
        break;
    }
}

void USBCapture::stop()
{
    emit(stopGrabbing());
}

void USBCapture::softwareTrigger()
{
    emit(captureNImages(nImagesToCapture));
}

void USBCapture::setExternalTrigger(bool hasExternalTrigger)
{
    Q_UNUSED(hasExternalTrigger);
}

void USBCapture::setUserOutput(bool value)
{
    Q_UNUSED(value);
}

void USBCapture::onCameraCommand(std::string command, double value)
{
    if (command == "exposure") {
        //std::cout << "EXPOSURE" << std::endl;
    }
}

void USBCapture::setTriggered(bool isTriggered)
{
    Q_UNUSED(isTriggered);
}

USBCapture::~USBCapture()
{
    if(isAvailable){
        captureThread->quit();
        cap->release();
        delete(cap);
    }
}
