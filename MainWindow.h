#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QLabel>
#include <QThread>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QFileInfo>
#include <QDesktopWidget>
#include <QValidator>
#include <QOpenGLWidget>
#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include "TimeConversion.h"
#include "CommandLineOptions.h"
#include "GuiOptions.h"
#include "RamMonitor.h"
#include "CameraEngine.h"
#include "Capture.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class TestApplication;

public:
    explicit MainWindow(Capture* capture, CommandLineOptions options, QWidget *parent = nullptr);
    void closeEvent(QCloseEvent *event);
    ~MainWindow();

signals:
    // To cameraEngine
    void videoOpen(QString fileName, int frameRate, cv::Size frameSize);
    void videoClose();
    void textFileOpen(QString fileName);
    void textFileClose();
    void setNImagesToRecord(long long);
    void openedFileName(QString fileName);
    void guiOptionsChanged(GuiOptions guiOptions);
    void guiClosing();

private slots:
    // Automatic connections
    void on_lineEdit_nFrames_editingFinished();
    void on_checkBox_record_clicked(bool checked);
    void on_pushButton_selectFile_clicked();
    void on_checkBox_externalTrigger_clicked(bool checked);
    void on_radioButton_continuous_clicked();
    void on_radioButton_burst_clicked();
    void on_checkBox_autoFileName_clicked(bool checked);
    void on_lineEdit_fileName_editingFinished();
    void on_checkBox_recordTracking_clicked(bool checked);
    void on_radioButton_frameWise_clicked();

    // From Ram Monitor
    void onAbout();
    void on_pushButton_selectTrackingFile_clicked();
    void on_lineEdit_trackingFileName_editingFinished();

private:
    Ui::MainWindow *ui;
    QLabel *label_recorded;
    QLabel *label_acquired;
    QLabel *label_statusSeparator;
    QLabel *label_availableRAM;
    QLabel *label_RAMDescription;
    QLabel *label_totalRAM;
    QLabel *label_RAMSeparator;
    QLabel *label_calculatedFrameRateDescription;
    QLabel *label_calculatedFrameRate;
    QString lineEditFileName = "tmp.avi";

    QString lineEditTrackingFileName = "tmp.csv";
    QString cameraInfo;
    QThread *ramMonitorThread = nullptr;
    RamMonitor* ramMonitor = nullptr;

    CameraEngine* cameraEngine;
    GuiOptions guiOptions;

    QGraphicsScene* scene;
    QGraphicsPixmapItem* pixmapItem;

    void resizeEvent(QResizeEvent* event);
    void showEvent(QShowEvent * event);
};

#endif // MAINWINDOW_H
