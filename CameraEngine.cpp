#include "CameraEngine.h"

Q_DECLARE_METATYPE(cv::Mat)

CameraEngine::CameraEngine(Capture* capture, CommandLineOptions options, QVBoxLayout* pluginLayout, QGraphicsScene* scene, QObject *parent) :
    QObject(parent), capture(capture)
{
    stateMachine = new QStateMachine(this);
    QState *initState = new QState(stateMachine);
    QState *idleState = new QState(stateMachine);
    QState *startState = new QState(stateMachine); //determines the kind of recording that happens
    QState *liveState = new QState(stateMachine);
    QState *recordingState = new QState(stateMachine);
    QState *manualStopState = new QState(stateMachine);
    QState *writingState = new QState(stateMachine); //wait for writing to finish

    qDebug()<< "OpenCV version : " << CV_VERSION;

    // Additional event datatypes
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<cv::Size>("cv::Size");
    qRegisterMetaType<std::vector<double>>("std::vector<double>");
    qRegisterMetaType<std::string>("std::string");

    // Initialize others
    guiUpdateTimer = new QTimer(this);
    guiUpdateTimer->setInterval(200);
    connect(guiUpdateTimer, &QTimer::timeout, this, &CameraEngine::onUpdateGui);
    guiUpdateTimer->start();

    socketInterface = new SocketInterface(this, options.hostname, options.port);

    if(options.gpu == -1){
        recordingWorker = new OpenCVRecorder();
    }else{
#ifdef WITH_FFMPEG
        recordingWorker = new FfmpegRecorder(nullptr, options.gpu);
#endif
    }
    recordingThread = new QThread(this);
    trackerRecordingWorker = new TrackerRecordingWorker(this);
    recordingWorker->codec = options.recordingCodec;
    recordingWorker->compression = options.compression;

    stateMachine->setInitialState(initState);

    initState->addTransition(this, &CameraEngine::initialized, idleState);
    idleState->addTransition(this, &CameraEngine::start, startState);
    idleState->addTransition(this, &CameraEngine::reinitialize, initState);
    idleState->addTransition(this, &CameraEngine::stop, initState);
    startState->addTransition(this, &CameraEngine::startRecording, recordingState);
    idleState->addTransition(capture, &Capture::startTimeStampEvent, startState);
    startState->addTransition(this, &CameraEngine::startLive, liveState);
    liveState->addTransition(this, &CameraEngine::stop, initState);
    liveState->addTransition(this, &CameraEngine::reinitialize, initState);
    liveState->addTransition(capture, &Capture::stopTimeStampEvent, initState);
    liveState->addTransition(this, &CameraEngine::lineInputEvent, initState);
    recordingState->addTransition(this, &CameraEngine::stop, manualStopState);
    manualStopState->addTransition(this, &CameraEngine::setImagesToRecord, writingState);
    recordingState->addTransition(capture, &Capture::stopTimeStampEvent, writingState);
    writingState->addTransition(recordingWorker, &Recorder::allWritten, initState);
    writingState->addTransition(this, &CameraEngine::stop, initState); //pressing stop twice

    // Associating different states with the associated workers
    connect(initState, &QState::entered, this, &CameraEngine::onStateInitEntered);
    connect(idleState, &QState::entered, this, &CameraEngine::onStateIdleEntered);
    connect(startState, &QState::entered, this, &CameraEngine::onStateStartEntered);
    connect(manualStopState, &QState::entered, this, &CameraEngine::onStateManualStopEntered);
    connect(recordingState, &QState::entered, this, &CameraEngine::onStateRecordingEntered);
    connect(writingState, &QState::entered, this, &CameraEngine::onStateWritingEntered);
    connect(liveState, &QState::entered, this, &CameraEngine::onStateLiveEntered);

    // Connection Main - Socket
    connect(socketInterface, &SocketInterface::statusInfo, this, &CameraEngine::statusInfo);
    connect(socketInterface, &SocketInterface::socketQuit, this, &CameraEngine::socketQuit);
    connect(this, &CameraEngine::guiClosing, socketInterface, &SocketInterface::onQuit);
    connect(this, &CameraEngine::stop, socketInterface, &SocketInterface::onStop);
    connect(this, &CameraEngine::openedFileName, socketInterface, &SocketInterface::onFileName);

    // The start signal is only forwared from socket if external trigger is off
    connect(socketInterface, &SocketInterface::socketStart, [this]() {
        if(!guiOptions.hasExternalTrigger){
            emit(start());
        }
    });

    //Socket signals to remote control the behavior
    idleState->addTransition(socketInterface, &SocketInterface::socketStop, initState);
    liveState->addTransition(socketInterface, &SocketInterface::socketStop, initState);
    recordingState->addTransition(socketInterface, &SocketInterface::socketStop, manualStopState);
    writingState->addTransition(socketInterface, &SocketInterface::socketStop, initState);//pressing stop twice

    // Connection Main - Capture
    connect(capture, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));
    connect(capture, &Capture::sendFrame, this, &CameraEngine::onFrameGrabbed); //only active if preview disabled
    connect(this, &CameraEngine::stop, capture, &Capture::stop);
    connect(this, &CameraEngine::start, capture, &Capture::softwareTrigger);

    // Connect external linestop signal (activated through command line arg)
    if (options.stopLine != 0){
        capture->stopLine = options.stopLine;
        connect(capture, &Capture::lineInputEvent, this, &CameraEngine::lineInputEvent);
    }

    // Calculate display interval based on command line arg
    if(int(options.displayFrameRate) == 0){
        displayInterval = 0;
    }else{
        displayInterval = int(ceil(float(capture->frameRate) / float(options.displayFrameRate)));
        frameRateInterval = int(ceil(float(capture->frameRate) / float(2)));
        if(displayInterval < 1){
            displayInterval = 1;
        }
        if(frameRateInterval < 1){
            frameRateInterval = 1;
        }
    }
    qDebug()<<"Showing every " << displayInterval << " frame (" << capture->frameRate  << "/" << options.displayFrameRate << ")";

    // Connection Main - Recorder
    connect(recordingWorker, &Recorder::frameWritten, [this](long long written) {
        this->written = written;} ); //store temporarily for gui update at interval

    connect(recordingWorker, &Recorder::statusInfo, this, &CameraEngine::statusInfo);

    connect(this, &CameraEngine::videoOpen, recordingWorker, &Recorder::onVideoOpen);
    connect(this, &CameraEngine::videoClose, recordingWorker, &Recorder::onVideoClose);
    connect(this, &CameraEngine::setNImagesToRecord, recordingWorker, &Recorder::onSetNImagesToGrab);

    // Connection Capture - Socket
    connect(capture, &Capture::timeStampEvent, socketInterface, &SocketInterface::onTimeStamp);
    connect(capture, &Capture::startTimeStampEvent, socketInterface, &SocketInterface::onStartTimeStamp);
    connect(capture, &Capture::stopTimeStampEvent, socketInterface, &SocketInterface::onStopTimeStamp);

    // Connection Capture - Recorder
    connect(capture, &Capture::sendFrame, recordingWorker, &Recorder::onFrameGrabbed);

    suffix = options.suffix;

    if(options.cameraIdx>=0){
        cameraIdxString = "cam" + QString::number(options.cameraIdx).append("_");
        autoFileNameTemplate.prepend(cameraIdxString);
        //autoTrackingFileName.prepend(cameraIdxString);
    }else{
        cameraIdxString = "cam" + capture->serialNumber.append("_");
        autoFileNameTemplate.prepend(cameraIdxString);
    }
    if(!suffix.isEmpty()){
        autoFileNameTemplate.append(suffix);
        //autoTrackingFileName.append(suffix);
    }

    colorConversion = capture->colorConversion; //for display
    nChannels = capture->nChannels;
    recordingWorker->colorConversion = capture->colorConversion; //for recording
    recordingWorker->nChannels = capture->nChannels;

    // Set the color table (used to translate colour indexes to qRgb values)
    for (int i=0; i<256; i++)
        colorTable.push_back(qRgb(i,i,i));

    // Initialize Tracking plugins
    QDir pluginsDir = QDir(qApp->applicationDirPath());

    #if defined(Q_OS_WIN)
        if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
            pluginsDir.cdUp();
    #elif defined(Q_OS_MAC)
        if (pluginsDir.dirName() == "MacOS") {
            pluginsDir.cdUp();
            pluginsDir.cdUp();
            pluginsDir.cdUp();
        }
    #endif
        pluginsDir.cd("plugins");

        foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
            QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
            QObject *plugin = loader.instance();
            if (plugin) {
                trackerInterface = qobject_cast <TrackerInterface* >(plugin);
                if(trackerInterface){ //Plugin successfully loaded
                    trackingThread = new QThread(this);
                    trackerRecordingWorker = new TrackerRecordingWorker(this);
                    //Plugin signals/slots currently need to be connected through old syntax
                    connect(capture, SIGNAL(sendFrame(cv::Mat)), trackerInterface->worker(), SLOT(onFrameGrabbed(cv::Mat)));
                    connect(trackerInterface->worker(), SIGNAL(trackingResult(std::vector<double>)), socketInterface, SLOT(onTrackingResult(std::vector<double>)));
                    connect(trackerInterface->worker(), SIGNAL(trackingResult(std::vector<double>)), trackerRecordingWorker, SLOT(onTrackingResult(std::vector<double>)));
                    connect(trackerInterface->worker(), SIGNAL(trackingMessage(std::string)), trackerRecordingWorker, SLOT(onTrackingMessage(std::string)));
                    connect(trackerInterface->worker(), SIGNAL(userOutput(bool)), capture, SLOT(setUserOutput(bool)));
                    connect(capture, SIGNAL(lineInputEvent(bool)), trackerInterface->worker(), SLOT(onUserInput(bool)));
                    connect(trackerInterface->worker(), SIGNAL(cameraCommand(std::string, double)), capture, SLOT(onCameraCommand(std::string, double)));
                    connect(socketInterface, SIGNAL(socketTrackingResult(std::vector<double>)), trackerInterface->worker(), SIGNAL(socketTrackingResult(std::vector<double>)));
                    connect(this, &CameraEngine::textFileOpen, trackerRecordingWorker, &TrackerRecordingWorker::onTextFileOpen);
                    connect(this, &CameraEngine::textFileClose, trackerRecordingWorker, &TrackerRecordingWorker::onTextFileClose);
                    trackerInterface->worker()->moveToThread(trackingThread);
                    trackerInterface->initializeUI(pluginLayout, scene, QSize(capture->frameSize.width, capture->frameSize.height));
                    trackingThread->start();

                    QTimer::singleShot(100, [this]() {emit(pluginLoaded(trackerInterface->getIsLogFileRecording()));}); //delayed signaling to update gui, since gui not yet ready

                    qDebug() << "Plugin: " << fileName << " loaded";
                    break;
                }
            }else{
                qDebug()<< "Failed to load plugin";
            }
        }

    // Move Recorder to separate Thread
    recordingWorker->moveToThread(recordingThread);
    connect(recordingThread, SIGNAL(finished()), recordingWorker, SLOT(deleteLater()));
    recordingThread->start();

    stateMachine->start();

    if (options.triggerOnStartup){
        QTimer::singleShot(100, [this]() {emit(start());}); //delayed signaling to send trigger on startup
    }

}

CameraEngine::~CameraEngine()
{
    stateMachine->stop();

    if(recordingThread){
        recordingThread->quit();
        recordingThread->wait();
    }

    if(trackingThread){
        trackingThread->quit();
        trackingThread->wait();
        //delete trackerInterface;
    }

//    delete recordingWorker;
//    delete pixmapItem;
//    delete ui;
}

double CameraEngine::getCameraFrameRate()
{
    return capture->frameRate;
}

void CameraEngine::onGuiOptionsChanged(GuiOptions guiOptions)
{
    qDebug()<<"GUI options changed!";

    if ( (this->guiOptions.captureMode != guiOptions.captureMode) |
         (this->guiOptions.hasExternalTrigger != guiOptions.hasExternalTrigger) |
         (this->guiOptions.nImagesToGrab != guiOptions.nImagesToGrab) |
         (this->guiOptions.isRecording != guiOptions.isRecording)){

        if(guiOptions.hasExternalTrigger){

        }

        this->guiOptions = guiOptions;
        emit reinitialize();
    }else{
        this->guiOptions = guiOptions;
    }

    if (guiOptions.fileNameAutoModeEnabled){
        emit newAutoFileName(guiOptions.filePath + '/' + autoFileNameTemplate + videoFileExtenstion,
                             guiOptions.trackingFilePath + '/' + autoFileNameTemplate + trackingFileExtenstion);
    }
}

void CameraEngine::onUpdateGui()
{
    emit frameIndexChanged(grabbed);
    emit writtenFrameIndexChanged(written);
}

void CameraEngine::onSocketFileNameChanged(std::string socketFileName)
{
    if (!guiOptions.fileNameAutoModeEnabled){ //socket name can only be used if no auto filename is enabled
        qDebug() << "Socket FileName changed: " << QString::fromStdString(socketFileName);


    }
}

void CameraEngine::onStateInitEntered()
{
    qDebug()<<"STATE: init";
    capture->setExternalTrigger(guiOptions.hasExternalTrigger);

    capture->init(guiOptions.captureMode, guiOptions.nImagesToGrab);
    emit initialized();
}

void CameraEngine::onStateIdleEntered()
{
    qDebug()<<"STATE: idle";
}

void CameraEngine::onStateStartEntered()
{
    grabbed = 0;
    processingTimer.restart();

    qDebug()<<"STATE: start";
    if (guiOptions.isRecording)
        emit startRecording();
    else{
        emit startLive();
    }
}

void CameraEngine::onStateRecordingEntered()
{
    qDebug()<<"STATE: recording";
    emit setNImagesToRecord(guiOptions.nImagesToGrab);

    QString fileName, trackingFileName;

    if(guiOptions.fileNameAutoModeEnabled){
        QString timeString = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
        fileName = guiOptions.filePath + "/" + cameraIdxString + timeString +
                suffix + videoFileExtenstion;
        trackingFileName = guiOptions.trackingFilePath + "/" + cameraIdxString + timeString +
                suffix + trackingFileExtenstion;
        emit newAutoFileName(fileName, trackingFileName);
    }else{
        fileName = guiOptions.filePath + '/' + guiOptions.fileName;
        trackingFileName = guiOptions.trackingFilePath + '/' + guiOptions.trackingFileName;
    }
    emit openedFileName(fileName); //to socket
    emit textFileOpen(trackingFileName);
    emit videoOpen(fileName, capture->frameRate, capture->frameSize);
}

void CameraEngine::onStateManualStopEntered()
{
    //Assure that all grabbed frames are being written
    qDebug()<< "CameraEngine: onStateManualStopEntered";
    emit setNImagesToRecord(grabbed);
    emit setImagesToRecord();
}

void CameraEngine::onStateWritingEntered()
{
    qDebug()<< "CameraEngine: onStateWritingEntered";
    emit statusInfo("Waiting for all frames");
    emit videoClose();
    emit textFileClose();
}

void CameraEngine::onStateLiveEntered()
{
    qDebug()<< "CameraEngine: onStateLiveEntered";
}

void CameraEngine::onTrackingPreviewChanged(bool checked)
{
    if(checked){
        disconnect(capture, &Capture::sendFrame, this, &CameraEngine::onFrameGrabbed); //only active if preview disabled
        connect(trackerInterface->worker(), SIGNAL(trackingPreview(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
        //connect(trackerInterface->worker(), &TrackerWorkerInterface::trackingPreview, this, &CameraEngine::onFrameGrabbed);
    }else{
        disconnect(trackerInterface->worker(), SIGNAL(trackingPreview(cv::Mat)), this, SLOT(onFrameGrabbed(cv::Mat)));
        connect(capture, &Capture::sendFrame, this, &CameraEngine::onFrameGrabbed); //only active if preview disabled
    }
}

// Called each time a frame is grabbed
void CameraEngine::onFrameGrabbed(const cv::Mat &im)
{
    grabbed++;
    frameRateGrabbed++;
    if(displayInterval>0){
        if (grabbed % displayInterval == 0){
            if (colorConversion > 0){
                cv::Mat cIm(cv::Size(im.rows, im.cols), CV_8UC3);
                cv::cvtColor(im, cIm, colorConversion);
                emit previewImage(convertImage(cIm));
            }else{
                emit previewImage(convertImage(im));
            }
        }
        if (frameRateGrabbed % frameRateInterval == 0){
            emit calculatedFrameRate(double(frameRateGrabbed) / processingTimer.elapsed()*1000);
            if(frameRateGrabbed == 100){
                frameRateGrabbed = 0;
                processingTimer.restart();
            }
        }
    }
}

// Convert openCV image to QImage
QImage CameraEngine::convertImage(const cv::Mat &mat)
{
    // 8-bits unsigned, NO. OF CHANNELS=1
    if(mat.type()==CV_8UC1){
        // Copy input Mat
        const uchar *qImageBuffer = static_cast<const uchar*>(mat.data);
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, QImage::Format_Indexed8);
        img.setColorTable(colorTable);
        return img;
    }
    // 8-bits unsigned, NO. OF CHANNELS=3
    if(mat.type()==CV_8UC3){
        // Copy input Mat
        const uchar *qImageBuffer = static_cast<const uchar*>(mat.data);
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, QImage::Format_RGB888);
        return img.rgbSwapped();
    }else{
        qDebug() << "ERROR: Mat could not be converted to QImage.";
        return QImage();
    }
}
