#ifndef CAMERAENGINE_H
#define CAMERAENGINE_H
#include <QStateMachine>
#include <QHistoryState>
#include <QObject>
#include <QPluginLoader>
#include <QDateTime>
#include <QTimer>
#include <QElapsedTimer>
#include <opencv2/core/core.hpp>
#include <pylon/PylonIncludes.h>
#include "CommandLineOptions.h"
#include "OpenCVRecorder.h"
#ifdef WITH_FFMPEG
#include "FfmpegRecorder.h"
#endif
#include "TrackerRecordingWorker.h"
#include "SocketInterface.h"
#include "Capture.h"
#include "TrackerInterface.h"
#include "GuiOptions.h"

class CameraEngine : public QObject
{
    Q_OBJECT
public:
    explicit CameraEngine(Capture* capture, CommandLineOptions options, QVBoxLayout* pluginLayout, QGraphicsScene* scene, QObject *parent = nullptr);
    ~CameraEngine();
    double getCameraFrameRate();
    QImage convertImage(const cv::Mat &mat);
private:
    QStateMachine *stateMachine; /* state machine */

    int displayInterval = 0;
    int frameRateInterval = 0;
    int frameRateGrabbed = 0; //temporary variable for framerate calculation
    long long grabbed = 0;
    long long written = 0;
    int colorConversion;
    int nChannels;
    GuiOptions guiOptions;

    QString cameraIdxString; // for naming of files only

    Capture *capture = nullptr;
    SocketInterface *socketInterface = nullptr;
    Recorder *recordingWorker = nullptr;
    TrackerRecordingWorker *trackerRecordingWorker = nullptr;
    QThread *recordingThread = nullptr;
    QThread *trackingThread = nullptr;
    TrackerInterface *trackerInterface = nullptr;

    QString autoFileNameTemplate = "yyyy-MM-dd-hh-mm-ss"; //prepended with cameraidx
    QString videoFileExtenstion = ".avi";
    QString trackingFileExtenstion = ".csv";
    QString suffix;

    QTimer *guiUpdateTimer; //for the status labels
    QElapsedTimer processingTimer; //framerate calculation
    QVector<QRgb> colorTable;

signals:
    void startRecording();
    void startLive();
    void start();
    void stop();
    void setImagesToRecord();
    void reinitialize(); //settings where changed that require reinit

    void initialized();
    void settingsChanged();

    //To Mainwindow
    void statusInfo(QString);
    void previewImage(QImage image);
    void frameIndexChanged(long long grabbed);
    void writtenFrameIndexChanged(long long written);
    void newAutoFileName(QString fileName, QString trackingFileName);
    void lineInputEvent(bool value);
    void pluginLoaded(bool isLogFileRecording);
    void socketQuit(quint64 timeStamp);

    //From MainWindow
    void calculatedFrameRate(double frameRate);
    void guiClosing();

    /*To Recorder*/
    void videoOpen(QString fileName, double frameRate, cv::Size frameSize);
    void videoClose();
    void textFileOpen(QString fileName);
    void textFileClose();
    void setNImagesToRecord(long long);
    void openedFileName(QString fileName);

public slots:
    void onStateInitEntered();
    void onStateIdleEntered();
    void onStateStartEntered();
    void onStateRecordingEntered();
    void onStateManualStopEntered();
    void onStateWritingEntered();
    void onStateLiveEntered();

    //From MainWindow
    void onGuiOptionsChanged(GuiOptions guiOptions);
    void onUpdateGui(); //only for status

    //From Socket
    void onSocketFileNameChanged(std::string socketFileName);

    // From Capture
    void onFrameGrabbed(const cv::Mat& mat);
//    void onBurstStarted(quint64 timeStamp);
//    void onBurstStopped(quint64 timeStamp);

    void onTrackingPreviewChanged(bool checked);

//    void onAllWritten();

};
#endif // CAMERAENGINE_H
