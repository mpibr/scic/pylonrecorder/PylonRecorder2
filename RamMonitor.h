#ifndef RAMMONITOR_H
#define RAMMONITOR_H

#include <QObject>
#include <QProcess>
#include <QTimer>

#if defined(Q_OS_WIN) // Windows (GlobalMemoryStatusEx)
    #include "Windows.h"
#endif

class RamMonitor : public QObject
{
    Q_OBJECT
public:
    explicit RamMonitor(QObject *parent = nullptr);
    int timerInterval = 1000; // in ms
    long totalRAM = 0;

signals:
    void availableRAM(long availableRam);

public slots:
    void onGetAvailableRAM(); // in MB
};

#endif // RAMMONITOR_H
