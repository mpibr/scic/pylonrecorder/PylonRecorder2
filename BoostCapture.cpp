#include "BoostCapture.h"

BoostCapture::BoostCapture(QObject *parent, int cameraIdx, uint stopLine, bool isTriggered, QString configFile, bool usbTweaksEnabled)
    : Capture(parent, stopLine, isTriggered), hasExternalTrigger(false)
{
    try{
        // Get the transport layer factory.
        Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();

        Pylon::DeviceInfoList_t devices;
        tlFactory.EnumerateDevices(devices, false);
        qDebug()<< "PylonRecorder found: " << devices.size() << " cameras!";
        if(devices.size()==0){
            throw RUNTIME_EXCEPTION("No compatible Basler cameras found");
        }

        if(cameraIdx >= 0){
            if(size_t(cameraIdx) < devices.size()){
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                }
            }else{
                throw RUNTIME_EXCEPTION("Camera index > detected cameras");
            }

        }else{
            bool isDeviceCreated = false;
            for(cameraIdx = 0; size_t(cameraIdx) < devices.size(); cameraIdx++){
                //Try to access first available camera
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                    isDeviceCreated = true;
                    break;
                }
            }
            if(!isDeviceCreated)
                throw RUNTIME_EXCEPTION("Could not connect to Basler camera (camera in use?)");

        }

        camera = new Pylon::CInstantCamera();
        camera->Attach(pDevice);

        // Print the model name of the camera.
        modelName = QString(camera->GetDeviceInfo().GetModelName());

        boostCaptureWorker = new BoostCaptureWorker(camera);
        connect(boostCaptureWorker, SIGNAL(sendFrame(cv::Mat)), this, SIGNAL(sendFrame(cv::Mat)));
        connect(this, SIGNAL(captureNImages(long long)), boostCaptureWorker, SLOT(onCaptureNImages(long long)));
        connect(this, SIGNAL(stopGrabbing()), boostCaptureWorker, SLOT(onStopGrabbing()));
        connect(boostCaptureWorker, SIGNAL(startTimeStampEvent(quint64)), this, SIGNAL(startTimeStampEvent(quint64)));
        connect(boostCaptureWorker, SIGNAL(stopTimeStampEvent(quint64)), this, SIGNAL(stopTimeStampEvent(quint64)));
        connect(this, SIGNAL(setCaptureMode(int)), boostCaptureWorker, SLOT(onSetCaptureMode(int)));

        captureThread = new QThread;
        boostCaptureWorker->moveToThread(captureThread);
        connect(boostCaptureWorker, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));

        // Register the standard configuration event handler for enabling software triggering.
        // The software trigger configuration handler replaces the default configuration
        // as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
        camera->RegisterConfiguration( new Pylon::CAcquireBurstSoftwareTriggerConfiguration, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);

        camera->Open();
        if(!configFile.isEmpty()){
            QFileInfo pfsFileInfo(configFile);

            if(pfsFileInfo.exists()){
                Pylon::CFeaturePersistence::Load( configFile.toLatin1().data(), &camera->GetNodeMap(), true );
                emit(statusInfo("Loaded configuration from " + configFile));
            }else{
                Pylon::CFeaturePersistence::Save( configFile.toLatin1().data(), &camera->GetNodeMap() );
            }
        }

        // Get Camera parameters
        GenApi::CIntegerPtr width = camera->GetNodeMap().GetNode("Width");
        GenApi::CIntegerPtr height = camera->GetNodeMap().GetNode("Height");
        frameSize = cv::Size(int(width->GetValue()), int(height->GetValue()));
        if (!bool(GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->GetValue())){
            emit(statusInfo("no constant fps set"));
            qDebug()<< "No constant fps set";
        }
        frameRate = uint(GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->GetValue());


        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("PixelFormat"))->FromString("BayerRG8"); //Select Line
        GenApi::CEnumerationPtr(camera->GetTLNodeMap().GetNode("PixelFormat"))->FromString("BayerRG8");
        GenApi::CEnumerationPtr(camera->GetTLNodeMap().GetNode("Format"))->FromString("BGR8");
        GenApi::CBooleanPtr(camera->GetTLNodeMap().GetNode("AutomaticFormatControl"))->FromString("false");

        GenApi::CEnumerationPtr pixelFormat = camera->GetNodeMap().GetNode("PixelFormat");
        QString pixelFormatString = "RGB8"; //hardcoded for now! currently only rgb8 supported

        if ( pixelFormatString.compare("BayerRG8")==0 ){
            colorConversion = cv::COLOR_BayerRG2RGB;
            nChannels = 1;
            qDebug() << "BoostCapture: converting from BayerRG8";
        }else if ( pixelFormatString.compare("BayerBG8")==0 ){
            colorConversion = cv::COLOR_BayerBG2RGB;
            nChannels = 1;
            qDebug() << "BoostCapture: converting from BayerBG8";
        }else if ( pixelFormatString.compare("RGB8")==0 ){
            colorConversion = 0;//cv::COLOR_BGR2RGB;
            //color conversion is done in the CaptureImageEventHandler
            boostCaptureWorker->isConversionEnabled = true;
            nChannels = 3;
            qDebug() << "BoostCapture: converting from BGR";
        }else if ( pixelFormatString.compare("Mono8")==0 ){
            colorConversion = 0;
            nChannels = 1;
            qDebug() << "BoostCapture: No conversion - using Mono";
        }
        boostCaptureWorker->nChannels = nChannels;

        // Low level usb tweaks
        if(usbTweaksEnabled){
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxNumBuffer"))->SetValue(8); //16
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxBufferSize"))->SetValue(131072); //131072
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxTransferSize"))->SetValue(16384); //65536 or 32768
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("NumMaxQueuedUrbs"))->SetValue(64); //64
            //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("TransferLoopThreadPriority"))->SetValue(5);
        }

        // For each output line, a specific UserOutput parameter is available to set the line as "user settable":
        // UserOutput 1 is available for output line Line 2
        //
        // UserOutput 2 is available for GPIO line Line 3 if the line is configured for output
        //
        // UserOutput 3 is available for GPIO line Line 4 if the line is configured for output.
        //
        // Set output line Line 2 to user settable

        qDebug()<< "BoostCapture: Testing LineModes to configure user output.";

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line4"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line4"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput3"))){
                    if(outputString.empty()){
                        //Line 4 is set for output
                        outputString = "UserOutput3";
                    }
                }
            //If this Line is not used as stopLine & trigger not set, use as trigger
            }else if(stopLine != 4 && triggerLine == 0){
                triggerLine = 4;
            }
        }


        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line3"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line3"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput2"))){
                    //Line 3 is set for output
                    outputString = "UserOutput2";
                }
            }else{
                if (stopLine != 3 && triggerLine == 0){ //If this Line is not used as stopLine, use as trigger
                    triggerLine = 3;
                }
            }
        }

        if (stopLine != 0){
            stopLineString = GenICam::gcstring(QString("Line").append(QString::number(stopLine)).toLatin1());
            boostCaptureWorker->stopLineString = stopLineString;
            qDebug() << "BoostCapture: External stop on" << stopLineString;
        }
        if (triggerLine != 0){
            triggerLineString = GenICam::gcstring(QString("Line").append(QString::number(triggerLine)).toLatin1());
            qDebug() << "BoostCapture: External trigger on" << triggerLineString;
        }

        qDebug() << "BoostCapture: User defined output on" << outputString;

        captureThread->start();
        isAvailable = true;
    }
    catch (GenICam::GenericException &e)
    {
        isAvailable = false;
        Pylon::PylonTerminate();
        // Error handling.
        std::cerr << "An exception occurred." << std::endl
                  << e.GetDescription() << std::endl;
    }
}

void BoostCapture::setFrameRate(double frameRate)
{
    GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(int(frameRate));
}

void BoostCapture::init(int captureMode, long long nImagesToCapture)
{
    // Start the grabbing using the grab loop thread, by setting the grabLoopType parameter
    // to GrabLoop_ProvidedByInstantCamera. The grab results are delivered to the image event handlers.
    // The GrabStrategy_OneByOne default grab strategy is used.
    emit(stop());
    camera->StopGrabbing();
    setTriggered(true);
    qDebug()<<"BoostCapture: Init";


    emit(setImagesToCapture(nImagesToCapture));
    emit(setCaptureMode(captureMode));
    emit(resetCounter());

    switch(captureMode){
    case CAPTURE_CONTINUOUS: {
        camera->StartGrabbing(Pylon::GrabStrategy_OneByOne);
        emit(captureNImages(0));
        qDebug()<<"BoostCapture: Initialized continuous grabbing";
        break;
    }
    case CAPTURE_BURST:
        if(nImagesToCapture>0){
            camera->StartGrabbing(size_t(nImagesToCapture), Pylon::GrabStrategy_OneByOne);
        }else{
            camera->StartGrabbing(Pylon::GrabStrategy_OneByOne);
        }
        emit(captureNImages(nImagesToCapture));
        qDebug()<<"BoostCapture: Initialized burst grabbing";
        break;
    case CAPTURE_FRAMEWISE:
        camera->StartGrabbing(Pylon::GrabStrategy_OneByOne);
        emit(captureNImages(0));
        qDebug()<<"BoostCapture: Initialized framewise grabbing";
        break;
    }

    emit(statusInfo("Camera initialized"));
}

void BoostCapture::stop()
{
    camera->StopGrabbing();
    emit(stopGrabbing());
}

void BoostCapture::softwareTrigger()
{
    if(!isTriggered && !hasExternalTrigger)
        camera->ExecuteSoftwareTrigger();
}

void BoostCapture::setExternalTrigger(bool hasExternalTrigger)
{
    if(triggerLine != 0){
        if(hasExternalTrigger){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString(triggerLineString);
            qDebug() << "BoostCapture: Enabled trigger on" << triggerLineString;
        }else{
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString("Software");
        }
        this->hasExternalTrigger = hasExternalTrigger;
    }
}

void BoostCapture::setUserOutput(bool value)
{
    if(!outputString.empty()){
        qDebug()<< "BoostCapture: User output " << value;
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("UserOutputSelector"))->FromString(outputString);
        GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("UserOutputValue"))->SetValue(value);
    }
    \
}

void BoostCapture::onCameraCommand(std::string command, double data)
{
    if (command == "exposure") {
        GenApi::CFloatPtr(camera->GetNodeMap().GetNode("ExposureTime"))->SetValue(int(data));
    }
}

void BoostCapture::setTriggered(bool isTriggered)
{
    if(isTriggered)
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "On");
    else
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");
}

BoostCapture::~BoostCapture()
{
    if(isAvailable){
        camera->StopGrabbing();
        camera->Close();

        if(pDevice->IsOpen()){
            pDevice->Close();
        }
        // Releases all pylon resources.
        Pylon::PylonTerminate();
    }

    qDebug()<<"Terminate";
}
