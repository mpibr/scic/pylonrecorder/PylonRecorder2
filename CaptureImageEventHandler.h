// Pylon specific image event handler.

#ifndef CAPTUREIMAGEEVENTHANDLER_H
#define CAPTUREIMAGEEVENTHANDLER_H
#include <QWidget>
#include <QDebug>
#include <opencv2/core/core.hpp>
#include <pylon/PylonIncludes.h>
#include <pylon/usb/_BaslerUsbCameraParams.h>

#define EVENT_TIMESTAMP 0
#define EVENT_START 1
#define EVENT_STOP 2

class CaptureImageEventHandler : public QObject, public Pylon::CImageEventHandler
{
    Q_OBJECT

public:
    explicit CaptureImageEventHandler(QObject* parent = nullptr);
    virtual void OnImageGrabbed( Pylon::CInstantCamera& camera, const Pylon::CGrabResultPtr& ptrGrabResult);
    GenICam::gcstring stopLineString;
    int nChannels = 3;
    bool isConversionEnabled = false;

private:
    Pylon::CImageFormatConverter formatConverter;
    // Create a PylonImage that will be used to create opencv images later.
    Pylon::CPylonImage pylonImage; //only used when converting
    int eventStatus = EVENT_TIMESTAMP;
    bool lineWasHigh = false;

signals:
    void imageEvent(const cv::Mat&);
    void startTimeStampEvent(quint64);
    void stopTimeStampEvent(quint64);
    void lineInputEvent(bool isHigh);
    void timeStampEvent(quint64);
    void statusInfo(QString info);

public slots:
    void onBurstStartEvent();
    void onBurstStopEvent();

};

#endif // CAPTUREIMAGEEVENTHANDLER_H
