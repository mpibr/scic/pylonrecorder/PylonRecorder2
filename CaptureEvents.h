#ifndef CAPTUREEVENTS_H
#define CAPTUREEVENTS_H

// Enumeration used for distinguishing different events.
enum CaptureEvents
{
    captureExposureEndEvent,           // Triggered on camera exposure
    captureFrameStartEvent,            // Triggered on Frame Start trigger
    captureFrameBurstStartEvent,       // Triggered on Burst start
    captureFrameBurstStartOvertrigger, // Trigger when already triggered
    captureFrameStartOvertrigger,      // Triggered by a camera event.
    captureImageReceivedEvent,         // Triggered by the receipt of an image.
    captureMoveEvent,                  // Triggered when the imaged item or the sensor head can be moved.
    captureNoEvent,                    // Used as default setting.
    captureEventOverrun,
};

#endif // CAPTUREEVENTS_H
