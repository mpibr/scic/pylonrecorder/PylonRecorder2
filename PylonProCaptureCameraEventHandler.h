//Pylon specific CCameraEventHandler

#ifndef PYLONPROCAPTURECAMERAEVENTHANDLER_H
#define PYLONPROCAPTURECAMERAEVENTHANDLER_H
#include <QWidget>
#include <QString>
#include <QDebug>
#include <pylon/PylonIncludes.h>
#include "CaptureEvents.h"

class PylonProCaptureCameraEventHandler :  public QObject, public Pylon::CCameraEventHandler
{
    Q_OBJECT

    enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

public:
    inline explicit PylonProCaptureCameraEventHandler(QObject* parent = nullptr) : QObject(parent){}
    virtual void OnCameraEvent( Pylon::CInstantCamera& camera, intptr_t userProvidedId, GenApi::INode* );

private:
    long long nImagesGrabbed = 0;
    long long nImagesToGrab = 0;
    int captureMode = CAPTURE_CONTINUOUS;

signals:
    void statusInfo(QString info);
    void burstStartEvent();
    void burstStopEvent();

public slots:
    void onResetCounter();
    void onSetImagesToGrab(long long);
    void onSetCaptureMode(int captureMode);

};

#endif // PYLONPROCAPTURECAMERAEVENTHANDLER_H
