// Deals with opening, closing and writing video files

#ifndef RECORDINGWORKER_H
#define RECORDINGWORKER_H
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QApplication> // To process event queue
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <QStorageInfo>
#include <QMessageBox>
#include "Recorder.h"

class OpenCVRecorder : public Recorder
{
    Q_OBJECT
public:
    explicit OpenCVRecorder(QObject *parent = nullptr);

public slots:
    void onFrameGrabbed(const cv::Mat &);
    void onVideoOpen(QString fileName, double frameRate, cv::Size frameSize);
    void onVideoClose();
    void onSetNImagesToGrab(long long);

private:
    void closeVideo();
    cv::VideoWriter videoWriter;
    long long nImagesToGrab = 0;
    long long nImagesGrabbed = 0;
    const int DiskFullWarningLimit = 1000; // Emit warning when less than this MB is available
};

#endif // RECORDINGWORKER_H
