#include "OpenCVRecorder.h"

OpenCVRecorder::OpenCVRecorder(QObject *parent)
    : Recorder(parent)
{
}

void OpenCVRecorder::onVideoOpen(QString fileName, double frameRate, cv::Size frameSize)
{
    if(!fileName.isEmpty()){

    QFileInfo fileInfo(fileName);

        QStorageInfo storage(fileInfo.absolutePath());
        if (storage.isReadOnly()){
            QMessageBox::warning(nullptr, tr("Warning"),
                                         QString("Storage location is read only!"));
            return;
        }

        qDebug() << "name:" << storage.name();
        qDebug() << "fileSystemType:" << storage.fileSystemType();
        qDebug() << "size:" << storage.bytesTotal()/1000/1000 << "MB";
        qDebug() << "availableSize:" << storage.bytesAvailable()/1000/1000 << "MB";

        if(storage.bytesAvailable()/1000/1000 < DiskFullWarningLimit){
            QMessageBox::warning(nullptr, tr("Warning"),
                                         QString("Less than ").append(QString::number(DiskFullWarningLimit).append("MB available for storage!")));
            return;
        }

        if(videoWriter.isOpened()){
            videoWriter.release();
        }

// This is a potential implementation for using gstreamer for streaming
//        QString gStreamerPipeline = QString("appsrc ! tee name=t ") +
//                "t. ! videoconvert ! videoscale ! queue ! x264enc pass=pass1 threads=4 bitrate=5000 tune=zerolatency  " +
//                "! queue ! flvmux streamable=true ! rtmpsink location=\"rtmp://127.0.0.1/live/stb\" max-lateness=100 " +
//                "t. ! video/x-raw,width=1280,height=720 ! appsink";
//        videoWriter.open(gStreamerPipeline.toStdString(), 0, frameRate, frameSize, true); //gstreamer pipeline

        if(compression > -1){
            videoWriter.set(cv::VIDEOWRITER_PROP_QUALITY, compression);
        }

        if(nChannels == 1 && colorConversion != cv::COLOR_BayerRG2RGB && colorConversion != cv::COLOR_BayerBG2RGB){
            videoWriter.open(fileName.toStdString(), codec, frameRate, frameSize, false);
        }else {
            videoWriter.open(fileName.toStdString(), codec, frameRate, frameSize, true);
        }

        if(!videoWriter.isOpened()){
            emit(statusInfo("Recorder: Unable to open file "));
            qDebug()<<"Recorder: Unable to open file "<<fileName;
        }else{
            emit(statusInfo("Recorder: Opened " + fileName));
            qDebug() << "Recorder: Opened " <<fileName;
            nImagesGrabbed = 0;
        }
    }
}

//called on each incoming frame
void OpenCVRecorder::onFrameGrabbed(const cv::Mat &im)
{
    if(videoWriter.isOpened()){
        if (colorConversion > 0){
            cv::Mat cIm;
            cv::cvtColor(im, cIm, colorConversion);
            videoWriter.write(cIm);
        }else{
            videoWriter.write(im);
        }

        nImagesGrabbed++;
        emit(frameWritten(nImagesGrabbed));

        if((nImagesToGrab>0) & (nImagesGrabbed>=nImagesToGrab)){
            closeVideo();
        }
    }
}

// Stay here until the video was fully written or closed
void OpenCVRecorder::onVideoClose(){
    if((nImagesToGrab>0) & (nImagesGrabbed>=nImagesToGrab)){
        closeVideo();
    }

    if(videoWriter.isOpened()){
        emit(statusInfo("Recorder: Waiting for all frames"));
        qDebug()<<"Recorder: Waiting for all frames";
    }
}

void OpenCVRecorder::onSetNImagesToGrab(long long nImagesToGrab){
    this->nImagesToGrab = nImagesToGrab;
    qDebug() << "Recorder: Number of frames to record was changed to " << nImagesToGrab;
}

void OpenCVRecorder::closeVideo()
{
    emit(statusInfo("Recorder: Closing Video file"));
    videoWriter.release();
    emit(statusInfo("Recorder: Video file closed"));
    qDebug() << "Recorder: Video file closed";
    emit(allWritten());
}
