#ifndef BOOSTCAPTUREWORKER_H
#define BOOSTCAPTUREWORKER_H
#include <opencv2/highgui/highgui.hpp>
#include <QApplication>
#include <QObject>
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QTime>
#include <QTimer>
#include <QApplication> // To process event queue
#include <pylon/PylonIncludes.h>
#include <pylon/usb/_BaslerUsbCameraParams.h>
#include "CaptureEvents.h"

class BoostCaptureWorker : public QObject
{
    Q_OBJECT

    enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

public:
    explicit BoostCaptureWorker(Pylon::CInstantCamera *camera = nullptr);

    Pylon::CInstantCamera *camera;
    long long nImagesToGrab;
    long long nImagesGrabbed;
    bool isRunning = false;
    QTime qTime;
    int nChannels = 3;
    bool isConversionEnabled = false;
    GenICam::gcstring stopLineString;
    int captureMode = CAPTURE_CONTINUOUS;

private:
    Pylon::CImageFormatConverter formatConverter;

public slots:
    void onCaptureNImages(long long nImages);
    void onStopGrabbing();
    void onCapture();
    void onSetCaptureMode(int captureMode);


signals:
    void startTimeStampEvent(quint64);
    void stopTimeStampEvent(quint64);
    void sendFrame(const cv::Mat&);
    void lineInputEvent(bool isHigh);
    void timeStampEvent(quint64);
    void statusInfo(QString info);

};

#endif // BOOSTCAPTUREWORKER_H
