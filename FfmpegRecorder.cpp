#ifdef WITH_FFMPEG

#include "FfmpegRecorder.h"

FfmpegRecorder::FfmpegRecorder(QObject *parent, int gpu)
    : Recorder(parent), gpu(gpu)
{
    qRegisterMetaType<cv::Mat>("cv::Mat");
    // initialize FFmpeg library

    //avformat_network_init();
    av_log_set_level(AV_LOG_DEBUG);
}

void FfmpegRecorder::closeFile()
{
    if ( !wasClosed && nImagesGrabbed>0){
        qDebug()<< "RecordingWorker: writing trailer";
        av_write_trailer(outputFormatContext);
        std::cout << nImagesGrabbed << " frames encoded" << std::endl;

        av_frame_free(&frame);
        if(avcodec_is_open(codecContext)){
            avcodec_close(codecContext);
        }
        avio_close(outputFormatContext->pb);
        avformat_free_context(outputFormatContext);
        emit(allWritten());
        emit(statusInfo("Recorder: All frames written"));
        wasClosed = true;
    }
}

void FfmpegRecorder::printAVErrorMessage(int returnCode)
{
    char buf[1024] = {0};
    av_strerror(returnCode, buf, 1024);
    qDebug() << buf;
}

void FfmpegRecorder::onFrameGrabbed(const cv::Mat &image)
{
    if ( isRecording ) {
        cv::Mat cImage;

        if (colorConversion>0){
            cv::cvtColor(image, cImage, colorConversion);
        }else {
            cImage = image;
        }

        // convert cv::Mat(OpenCV) to AVFrame(FFmpeg)
        const int stride[] = { static_cast<int>(cImage.step[0]) };
        sws_scale(swsContext, &cImage.data, stride, 0, cImage.rows, frame->data, frame->linesize);

        frame->pts = frame_pts++;

        AVPacket* pkt;
        // encode video frame
        pkt = av_packet_alloc();

        pkt->data = nullptr;
        pkt->size = 0;
        int ret;
        bool got_pkt = false;

        while (!got_pkt) {
            if (!isRecording) {
                av_frame_unref(frame);
                frame->pts++;
            }

            ret = avcodec_send_frame(codecContext, !isRecording ? nullptr : frame);
            if (ret < 0) {
                std::cerr << "fail to avcodec_send_frame: ret=" << ret << std::endl;
                printAVErrorMessage(ret);
                return;
            }

            ret = avcodec_receive_packet(codecContext, pkt);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                // No packet available or encoding finished
                continue;
            } else if (ret < 0) {
                std::cerr << "fail to avcodec_receive_packet: ret=" << ret << std::endl;
                printAVErrorMessage(ret);
                return;
            }

            got_pkt = true;
        }

        ++nImagesGrabbed;

        if (got_pkt) {
            // rescale packet timestamp
            pkt->duration = 1;
            av_packet_rescale_ts(pkt, codecContext->time_base, vstream->time_base);
            // write packet
            av_write_frame(outputFormatContext, pkt);
        } else {
            std::cout << "RecordingWorker: frame " << nImagesGrabbed << " was empty" << std::endl;
        }

        //std::cout << "RecordingWorker: writing frame:" << nImagesGrabbed << '\r' << std::flush;  // dump progress
        emit(frameWritten(nImagesGrabbed));

        av_packet_unref(pkt);

        if((nImagesToGrab != 0) && (nImagesGrabbed >= nImagesToGrab)){
            isRecording = false;
        }

    }else{
        closeFile();
    }

}

void FfmpegRecorder::onVideoClose()
{
    if((nImagesToGrab != 0) && (nImagesGrabbed >= nImagesToGrab)){
        isRecording = false;
        closeFile();
    }

    if(isRecording){
        emit(statusInfo("Recorder: Waiting for all frames"));
        qDebug()<<"Recorder: Waiting for all frames";
    }

    qDebug()<< "onVideoClose" << nImagesGrabbed << "/" << nImagesToGrab;
}

void FfmpegRecorder::onSetNImagesToGrab(long long nImagesToGrab)
{
    this->nImagesToGrab = nImagesToGrab;
    qDebug() << "Recorder: Number of frames to record was changed to " << nImagesToGrab;

    qDebug()<< "onSetNImagesToGrab" << nImagesGrabbed << "/" << nImagesToGrab;
}

void FfmpegRecorder::onVideoOpen(QString fileName, double frameRate, cv::Size frameSize)
{
    QByteArray fileNameByteArray = fileName.toLatin1();
    const char *outfile = fileNameByteArray.data();

    QByteArray codecByteArray = ffmpeg_codec.toLatin1();
    const char *avCodec = codecByteArray.data();

    const AVRational dst_fps = {int(frameRate), 1};

    // open output format context
    outputFormatContext = nullptr;
    int ret = avformat_alloc_output_context2(&outputFormatContext, nullptr, nullptr, outfile);
    if (ret < 0) {
        std::cerr << "fail to avformat_alloc_output_context2(" << outfile << "): ret=" << ret << std::endl;
        printAVErrorMessage(ret);
        return;
    }

    // create new video stream
    vcodec = avcodec_find_encoder_by_name(avCodec);
    vstream = avformat_new_stream(outputFormatContext, vcodec);
    if (!vstream) {
        std::cerr << "fail to avformat_new_stream" << std::endl;
        return;
    }
    vstream->r_frame_rate = vstream->avg_frame_rate = dst_fps;


    // Allocate and initialize the codec context
    codecContext = avcodec_alloc_context3(vcodec);
    if (!codecContext) {
        std::cerr << "failed to allocate codec context" << std::endl;
        return;
    }
    codecContext->width = frameSize.width;
    codecContext->height = frameSize.height;
    codecContext->time_base = vstream->time_base = av_inv_q(dst_fps);
    codecContext->pix_fmt = vcodec->pix_fmts[0];

    // Set codec specific parameters
    ret = av_opt_set(codecContext->priv_data, "gpu", QString::number(gpu).toLatin1().data(), 0);
    if (ret < 0) {
          printAVErrorMessage(ret);
    }else{
       qDebug()<< "Using gpu: "<< QString::number(gpu);
    }
    ret = av_opt_set(codecContext->priv_data, "profile", "main", 0);
    if (ret < 0) {
       printAVErrorMessage(ret);
    }
    ret = av_opt_set(codecContext->priv_data, "preset", "p1", 0);
    if (ret < 0) {
        printAVErrorMessage(ret);
    }
    ret = av_opt_set(codecContext->priv_data, "rc", "constqp", 0);
    if (ret < 0) {
        printAVErrorMessage(ret);
    }

    // Open the codec for the stream
    ret = avcodec_open2(codecContext, vcodec, nullptr);
    if (ret < 0) {
        std::cerr << "Error opening codec." << std::endl;
        printAVErrorMessage(ret);
        return;
    }else{
        qDebug()<<"RecordingWorker:  Opened video encoder with codec " << ffmpeg_codec;
    }

    // Use avcodec_parameters_from_context to initialize codecParameters
    avcodec_parameters_from_context(vstream->codecpar, codecContext);

    std::cout
        << "outfile: " << outfile << "\n"
        << "format:  " << outputFormatContext->oformat->name << "\n"
        << "vcodec:  " << vcodec->name << "\n"
        << "size:    " << frameSize.width << 'x' << frameSize.height << "\n"
        << "fps:     " << av_q2d(dst_fps) << "\n"
        << "pixfmt:  " << av_get_pix_fmt_name(codecContext->pix_fmt) << "\n"
        << std::flush;

    // initialize sample scaler
    if(colorConversion==cv::COLOR_BayerRG2RGB || colorConversion==cv::COLOR_BayerBG2RGB || nChannels != 1){
        swsContext = sws_getCachedContext(
            nullptr, frameSize.width, frameSize.height, AV_PIX_FMT_BGR24,
            frameSize.width, frameSize.height,codecContext->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);
    }else{ //nChannels == 1
        swsContext = sws_getCachedContext(
            nullptr, frameSize.width, frameSize.height, AV_PIX_FMT_GRAY8,
            frameSize.width, frameSize.height, codecContext->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);
    };

    if (!swsContext) {
        std::cerr << "fail to sws_getCachedContext" << std::endl;
        return;
    }else{
        qDebug()<<"RecordingWorker: Initialized sample scaler";
    }

    // prepare frame
    frame = av_frame_alloc();
    if (!frame) {
        std::cerr << "Error allocating destination frame." << std::endl;
        return;
    }
    frame->width = frameSize.width;
    frame->height = frameSize.height;
    frame->format = static_cast<int>(codecContext->pix_fmt);

    // Allocate memory for the data buffer
    int alignment = 32; //1, 2, 4, 8, 16, 32, 64 etc
    ret = av_frame_get_buffer(frame, alignment);
    if (ret < 0) {
        std::cerr << "Error allocating frame data buffer." << std::endl;
        av_frame_free(&frame); // Clean up on error
        printAVErrorMessage(ret);
        return;
    }

    // open file
    ret = avio_open2(&outputFormatContext->pb, outfile, AVIO_FLAG_WRITE, nullptr, nullptr);
    if (ret < 0) {
        std::cerr << "fail to avio_open2: ret=" << ret << std::endl;
        printAVErrorMessage(ret);
        return;
    }

    // encoding loop
    ret = avformat_write_header(outputFormatContext, nullptr);
    if (ret < 0) {
        std::cerr << "failed to write header: ret=" << ret << std::endl;
        printAVErrorMessage(ret);
    }

    frame_pts = 0;
    nImagesGrabbed = 0;

    isRecording = true;
    wasClosed = false;

    qDebug()<< "RecordingWorker: Initialized NVENC encoder";
}

#endif
