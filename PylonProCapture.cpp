#include "PylonProCapture.h"

PylonProCapture::PylonProCapture(QObject *parent, int cameraIdx, uint stopLine, bool isTriggered, QString configFile, bool usbTweaksEnabled)
    : Capture(parent, stopLine, isTriggered)
{
    try{
        // Get the transport layer factory.
        Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();

        Pylon::DeviceInfoList_t devices;
        tlFactory.EnumerateDevices(devices, false);
        qDebug()<< "PylonRecorder found: " << devices.size() << " cameras!";
        if(devices.size()==0){
            throw RUNTIME_EXCEPTION("No compatible Basler cameras found");
        }

        if(cameraIdx >= 0){
            if(size_t(cameraIdx) < devices.size()){
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                }
            }else{
                throw RUNTIME_EXCEPTION("Camera index > detected cameras");
            }

        }else{
            bool isDeviceCreated = false;
            for(cameraIdx = 0; size_t(cameraIdx) < devices.size(); cameraIdx++){
                //Try to access first available camera
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                    isDeviceCreated = true;
                    break;
                }
            }
            if(!isDeviceCreated)
                throw RUNTIME_EXCEPTION("Could not connect to Basler camera (camera in use?)");

        }

        camera = new Pylon::CInstantCamera();
        camera->Attach(pDevice);

        // Print the model name of the camera.
        modelName = QString(camera->GetDeviceInfo().GetModelName());
        friendlyName = QString(camera->GetDeviceInfo().GetFriendlyName());
        serialNumber = QString(camera->GetDeviceInfo().GetSerialNumber());

        // When using the grab loop thread provided by the Instant Camera object, an image event handler processing the grab
        // results must be created and registered.
        cImageEventGrabber = new PylonProCaptureImageEventHandler(this);
        cCameraEventHandler = new PylonProCaptureCameraEventHandler(this);

        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::imageEvent, this, &PylonProCapture::sendFrame);
        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::startTimeStampEvent, this, &PylonProCapture::startTimeStampEvent);
        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::stopTimeStampEvent, this, &PylonProCapture::stopTimeStampEvent); //this signal is not emitted reliably (hence the cCameraEventHandler is also used for stopping)
        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::timeStampEvent, this, &PylonProCapture::timeStampEvent);
        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::statusInfo, this, &PylonProCapture::statusInfo);
        connect(cImageEventGrabber, &PylonProCaptureImageEventHandler::lineInputEvent, this, &PylonProCapture::lineInputEvent);

        connect(cCameraEventHandler, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));

        // These events are forwarded to add a timeStamp
        connect(cCameraEventHandler, &PylonProCaptureCameraEventHandler::burstStartEvent, cImageEventGrabber, &PylonProCaptureImageEventHandler::onBurstStartEvent);
        connect(cCameraEventHandler, &PylonProCaptureCameraEventHandler::burstStopEvent, cImageEventGrabber, &PylonProCaptureImageEventHandler::onBurstStopEvent);
        connect(cCameraEventHandler, &PylonProCaptureCameraEventHandler::burstStopEvent, this, [this](){
                emit stopTimeStampEvent(0);
            });

        connect(this, &PylonProCapture::setCaptureMode, cCameraEventHandler, &PylonProCaptureCameraEventHandler::onSetCaptureMode);
        connect(this, &PylonProCapture::setImagesToCapture, cCameraEventHandler, &PylonProCaptureCameraEventHandler::onSetImagesToGrab);
        connect(this, &PylonProCapture::resetCounter, cCameraEventHandler, &PylonProCaptureCameraEventHandler::onResetCounter);

        // Register events
        camera->RegisterImageEventHandler( cImageEventGrabber, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);

        // FrameStart is not available on some cameras. Try to use ExposureEnd instead
        frameEventName = "FrameStart";
        if(!GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName(frameEventName))){
            frameEventName = "ExposureEnd";
        }

        camera->RegisterCameraEventHandler( cCameraEventHandler, Pylon::String_t("Event").append(frameEventName), captureFrameStartEvent, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_None);

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStartOvertrigger"))){
            camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameBurstStartOvertrigger"))){
            camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameBurstStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);
        }


        // Camera event processing must be activated first, the default is off.
        camera->GrabCameraEvents = true;

        camera->Open();
        if(!configFile.isEmpty()){
            QFileInfo pfsFileInfo(configFile);

            if(pfsFileInfo.exists()){
                Pylon::CFeaturePersistence::Load( configFile.toLatin1().data(), &camera->GetNodeMap(), true );
                emit statusInfo("Loaded configuration from " + configFile);
            }else{
                Pylon::CFeaturePersistence::Save( configFile.toLatin1().data(), &camera->GetNodeMap() );
            }
        }

        // Set Camera parameters
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString(frameEventName);
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameBurstStart"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStart");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameStartOvertrigger"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameStartOvertrigger");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameBurstStartOvertrigger"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStartOvertrigger");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        // Use fixed FrameRate
        //GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->SetValue(true);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Width"))->SetValue(width);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Height"))->SetValue(width);
        //GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(frameRate);

        // Get Camera parameters
        GenApi::CIntegerPtr width = camera->GetNodeMap().GetNode("Width");
        GenApi::CIntegerPtr height = camera->GetNodeMap().GetNode("Height");
        frameSize = cv::Size(int(width->GetValue()), int(height->GetValue()));
        if (!bool(GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->GetValue())){
            emit statusInfo("no constant fps set");
            qDebug()<< "No constant fps set";
        }
        frameRate = uint(GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->GetValue());

        GenApi::CEnumerationPtr pixelFormat = camera->GetNodeMap().GetNode("PixelFormat");
        QString pixelFormatString = QString::fromStdString(std::string(pixelFormat->ToString())); //using string since enums vary between USB and GigE

        if ( pixelFormatString.compare("BayerRG8")==0 ){
            colorConversion = cv::COLOR_BayerRG2RGB;
            nChannels = 1;
            qDebug() << "PylonCapture: converting from BayerRG8";
        }else if ( pixelFormatString.compare("BayerBG8")==0 ){
            colorConversion = cv::COLOR_BayerBG2RGB;
            nChannels = 1;
            qDebug() << "PylonCapture: converting from BayerBG8";
        }else if ( pixelFormatString.compare("RGB8")==0 ){
            colorConversion = 0;//cv::COLOR_BGR2RGB;
            //color conversion is done in the CaptureImageEventHandler
            cImageEventGrabber->isConversionEnabled = true;
            nChannels = 3;
            qDebug() << "PylonCapture: converting from BGR";
        }else if ( pixelFormatString.compare("Mono8")==0 ){
            colorConversion = 0;
            nChannels = 1;
            qDebug() << "PylonCapture: No conversion - using Mono";
        }
        cImageEventGrabber->nChannels = nChannels;


        // Low level usb tweaks
        if(usbTweaksEnabled){
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxNumBuffer"))->SetValue(8); //16
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxBufferSize"))->SetValue(131072); //131072
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxTransferSize"))->SetValue(16384); //65536 or 32768
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("NumMaxQueuedUrbs"))->SetValue(64); //64
            //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("TransferLoopThreadPriority"))->SetValue(5);
        }

        // For each output line, a specific UserOutput parameter is available to set the line as "user settable":
        // UserOutput 1 is available for output line Line 2
        //
        // UserOutput 2 is available for GPIO line Line 3 if the line is configured for output
        //
        // UserOutput 3 is available for GPIO line Line 4 if the line is configured for output.
        //
        // Set output line Line 2 to user settable

        qDebug()<< "PylonCapture: Testing LineModes to configure user output.";

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line4"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line4"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput3"))){
                    if(outputString.empty()){
                        //Line 4 is set for output
                        outputString = "UserOutput3";
                    }
                }
            //If this Line is not used as stopLine & trigger not set, use as trigger
            }else if(stopLine != 4 && triggerLine == 0){
                triggerLine = 4;
            }
        }


        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line3"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line3"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput2"))){
                    //Line 3 is set for output
                    outputString = "UserOutput2";
                }
            }else{
                if (stopLine != 3 && triggerLine == 0){ //If this Line is not used as stopLine, use as trigger
                    triggerLine = 3;
                }
            }
        }


        if (stopLine != 0){
            stopLineString = GenICam::gcstring(QString("Line").append(QString::number(stopLine)).toLatin1());
            cImageEventGrabber->stopLineString = stopLineString;
            qDebug() << "PylonCapture: External stop on" << stopLineString;
        }

        if (triggerLine != 0){
            triggerLineString = GenICam::gcstring(QString("Line").append(QString::number(triggerLine)).toLatin1());
            qDebug() << "PylonCapture: External trigger on" << triggerLineString;
        }

        qDebug() << "PylonCapture: User defined output on" << outputString;

        isAvailable = true;
    }
    catch (GenICam::GenericException &e)
    {
        isAvailable = false;
        Pylon::PylonTerminate();
        // Error handling.
        std::cerr << "An exception occurred." << std::endl
                  << e.GetDescription() << std::endl;
    }
}

void PylonProCapture::setFrameRate(double frameRate)
{
    GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(int(frameRate));
}

void PylonProCapture::init(int captureMode, long long nImagesToCapture)
{
    // Start the grabbing using the grab loop thread, by setting the grabLoopType parameter
    // to GrabLoop_ProvidedByInstantCamera. The grab results are delivered to the image event handlers.
    // The GrabStrategy_OneByOne default grab strategy is used.
    camera->StopGrabbing();
    //qDebug()<< GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->GetCurrentEntry()->ToString();

    GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSelector"))->FromString("FrameStart");
    GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");
    GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSelector"))->FromString("FrameBurstStart");
    GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");

    emit setImagesToCapture(nImagesToCapture);
    emit setCaptureMode(captureMode);
    emit resetCounter();
    switch(captureMode){
    case CAPTURE_CONTINUOUS:
        setTriggered(false);
        //camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized continuous grabbing";
        break;
    case CAPTURE_BURST:
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSelector"))->FromString("FrameBurstStart");
        setTriggered(true);
        if(nImagesToCapture>0){
            camera->StartGrabbing(size_t(nImagesToCapture), Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }else{
            camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }
        qDebug()<<"PylonCapture: Initialized burst grabbing";
        break;
    case CAPTURE_FRAMEWISE:
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSelector"))->FromString("FrameStart");
        setTriggered(true);
        camera->StartGrabbing( Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized framewise grabbing";
        break;
    }
    this->captureMode = captureMode;

    emit statusInfo("Camera initialized");
}

void PylonProCapture::stop()
{
    camera->StopGrabbing();
}

void PylonProCapture::softwareTrigger()
{
    if(captureMode==CAPTURE_CONTINUOUS){
        if(!camera->IsGrabbing())
            camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
    }else{
        if(!isTriggered)
            camera->ExecuteSoftwareTrigger();
    }
}

void PylonProCapture::setExternalTrigger(bool hasExternalTrigger)
{
    this->hasExternalTrigger = hasExternalTrigger;
}

void PylonProCapture::setUserOutput(bool value)
{
    if(!outputString.empty()){
        qDebug()<< "PylonCapture: User output " << value;
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("UserOutputSelector"))->FromString(outputString);
        GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("UserOutputValue"))->SetValue(value);
    }
}

void PylonProCapture::onCameraCommand(std::string command, double data)
{
    if (command == "exposure") {
        GenApi::CFloatPtr(camera->GetNodeMap().GetNode("ExposureTime"))->SetValue(int(data));
    }
}

void PylonProCapture::setTriggered(bool isTriggered)
{
    if(isTriggered)
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "On");
    else
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");

    if(hasExternalTrigger && triggerLine != 0){
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString(triggerLineString);
        qDebug() << "PylonCapture: Enabled trigger on" << triggerLineString;
    }else{
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString("Software");
        qDebug() << "PylonCapture: Enabled SoftwareTrigger";
    }
}

PylonProCapture::~PylonProCapture()
{
    if(isAvailable){
        camera->StopGrabbing();
        camera->Close();
        camera->DeregisterImageEventHandler(cImageEventGrabber);

        camera->DeregisterCameraEventHandler(cCameraEventHandler, Pylon::String_t("Event").append(frameEventName));

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStartOvertrigger"))){
            camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameStartOvertrigger");
        }
        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameBurstStartOvertrigger"))){
            camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameBurstStartOvertrigger");
        }

        if(pDevice->IsOpen()){
            pDevice->Close();
        }
        // Releases all pylon resources.
        Pylon::PylonTerminate();
    }

    qDebug()<<"Terminate";
}
