// Deals with opening, closing and writing text files

#ifndef TRACKERRECORDINGWORKER_H
#define TRACKERRECORDINGWORKER_H
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QFile>
#include <opencv2/core/core.hpp>

class TrackerRecordingWorker : public QObject
{
    Q_OBJECT
public:
    explicit TrackerRecordingWorker(QObject* parent = nullptr);

signals:
    void statusInfo(QString info);

public slots:
    void onTrackingResult(std::vector<double> data);
    void onTrackingMessage(std::string message);
    void onTextFileOpen(QString fileName);
    void onTextFileClose();

private:
    QFile logFile;
};

#endif // RECORDINGWORKER_H
