#ifndef COMMANDLINEOPTIONS_H
#define COMMANDLINEOPTIONS_H
#include <QString>

struct CommandLineOptions{
    int wndX;
    int wndY;
    int wndW;
    int wndH;
    double displayFrameRate;
    QString defaultPath;
    int cameraIdx;
    QString configFile;
    int recordingCodec;
    double compression;
    bool autoFileName;
    bool record;
    uint stopLine;
    bool isTriggered;
    int captureMode;
    uint framesToCapture;
    QString suffix;
    int socketType;
    QString hostname;
    uint port;
    bool usbTweaksEnabled;
    int gpu;
    bool cxp;
    bool triggerOnStartup;
};

#endif // COMMANDLINEOPTIONS_H
