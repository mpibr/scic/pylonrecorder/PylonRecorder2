// Very simple interface to use USB cameras through OpenCV

#ifndef USBCAPTURE_H
#define USBCAPTURE_H
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <QFileInfo>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include "Capture.h"
#include "USBCaptureWorker.h"

class USBCapture : public Capture
{
    Q_OBJECT

public:
    explicit USBCapture(QObject *parent = nullptr, int cameraIdx = 0, uint stopLine = 0, bool isTriggered = 0, QString configFile = "");
    ~USBCapture();

    virtual void init(int captureMode, long long nImagesToCapture);
    virtual void stop();
    virtual void softwareTrigger();
    virtual void setFrameRate(double);
    virtual void setTriggered(bool);
    virtual void setExternalTrigger(bool);
    virtual void setUserOutput(bool);
    virtual void onCameraCommand(std::string, double);
    void setExposure(double exposure);


private:
    // Required for parsing of configuration file
    QMap<QString, int> cvProperties;

    // Pylon devices
    cv::VideoCapture *cap;
    USBCaptureWorker *usbCaptureWorker;
    QThread *captureThread;
    long long nImagesToCapture;

signals:
    void captureNImages(long long);
    void stopGrabbing();
    void updateFrameRate(double);

};

#endif // USBCAPTURE_H
