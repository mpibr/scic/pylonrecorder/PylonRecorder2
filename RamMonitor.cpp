#include "RamMonitor.h"

RamMonitor::RamMonitor(QObject *parent) : QObject(parent)
{
#if defined(Q_OS_WIN) //Windows (GlobalMemoryStatusEx)
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    if (GlobalMemoryStatusEx(&memory_status)) {
      totalRAM = memory_status.ullTotalPhys / (1024 * 1024);
    }
#elif defined(Q_OS_LINUX) //Linux (/proc/meminfo)
    QProcess p;
    p.start("awk", QStringList() << "/MemTotal/ { print $2 }" << "/proc/meminfo");
    p.waitForFinished();
    QString memory = p.readAllStandardOutput();
    p.close();
    totalRAM = memory.toLong() / 1024;
#elif defined(Q_OS_MAC) //Mac (sysctl)
    QProcess p;
    p.start("sysctl", QStringList() << "kern.version" << "hw.memsize");
    p.waitForFinished();
    QString system_info = p.readAllStandardOutput();
    //comes back as: hw.memsize:
    p.close();
    totalRAM = memory.toLong() / (1024*1024); //UNTESTED!!
#endif

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(onGetAvailableRAM()));
    timer->start(timerInterval); //check for RAM every second
}

void RamMonitor::onGetAvailableRAM()
{
#if defined(Q_OS_WIN) //Windows (GlobalMemoryStatusEx)
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    if (GlobalMemoryStatusEx(&memory_status)) {
      emit(availableRAM(memory_status.ullAvailPhys / (1024 * 1024)));
    }
#elif defined(Q_OS_LINUX) //Linux (/proc/meminfo)
    QProcess p;
    p.start("awk", QStringList() << "/MemFree/ { print $2 }" << "/proc/meminfo");
    p.waitForFinished();
    QString memory = p.readAllStandardOutput();
    p.close();
    emit(availableRAM(memory.toLong() / 1024));
#elif defined(Q_OS_MAC) //Mac (sysctl)
    QProcess p;
    p.start("sysctl", QStringList() << "kern.version" << "hw.memsize");
    p.waitForFinished();
    QString system_info = p.readAllStandardOutput();
    //comes back as: hw.memsize:
    p.close();
    emit(availableRAM(memory.toLong() / (1024*1024)); //UNTESTED!!
#endif
}
