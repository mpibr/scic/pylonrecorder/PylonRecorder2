#include "BoostCaptureWorker.h"

BoostCaptureWorker::BoostCaptureWorker(Pylon::CInstantCamera *camera) : camera(camera)
{
    formatConverter.OutputPixelFormat = Pylon::PixelType_BGR8packed;
}

void BoostCaptureWorker::onCaptureNImages(long long nImagesToGrab)
{
    qDebug()<<"onCaptureNImages";
    this->nImagesToGrab = nImagesToGrab;
    nImagesGrabbed = 0;
    isRunning = true;
    this->onCapture();
}


void BoostCaptureWorker::onStopGrabbing()
{
    qDebug()<<"Stop";
    isRunning = false;
    camera->StopGrabbing();

}

void BoostCaptureWorker::onCapture()
{
try{
    if (( camera->IsGrabbing() & isRunning) ) {
        //std::cout << "Grabbing." << std::endl << std::endl;

        if(nImagesGrabbed == 1){
            if(captureMode!=CAPTURE_FRAMEWISE){
                qDebug()<< "Trigger off";
                GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");
            }
            emit(startTimeStampEvent(quint64(qTime.elapsed())));
        }

        Pylon::CGrabResultPtr ptrGrabResult;
        //also defined in Pylon header but for some reason not working. INFINITE=0xFFFFFFFF
        camera->RetrieveResult(0xFFFFFFFF, ptrGrabResult, Pylon::TimeoutHandling_ThrowException);

        // Image grabbed succ essfully?
        if (ptrGrabResult.IsValid() && ptrGrabResult->GrabSucceeded()){
            //Note that all images have to be cloned since internally they have the reference to ptrGrabResult
            switch (nChannels){
            // Create an OpenCV image from a pylon image.
            case 1:
                emit(sendFrame(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC1, ptrGrabResult->GetBuffer()).clone()));
                break;
            case 3:
                if(isConversionEnabled){ //RGB to BGR is much faster in Pylon
                    emit(sendFrame(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *) ptrGrabResult->GetBuffer()).clone()));
                }else{
                    //Currently not used
                    emit(sendFrame(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, ptrGrabResult->GetBuffer()).clone()));
                }
                break;
            }

            nImagesGrabbed++;
            ptrGrabResult.Release();

            // The event of the last frame should be received before the last frame
            if(((nImagesToGrab>0) & (nImagesGrabbed == nImagesToGrab)) | (nImagesGrabbed == std::numeric_limits <long long>::max())){
                isRunning = false;
                emit(stopTimeStampEvent(quint64(qTime.elapsed())));
            }

        }else{
            //I have not found a solution without eventing to determine if camera was triggered. Hence this exception.
            std::cout << "Error while grabbing: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
        }

    }else{
        isRunning = false;
    }
    }catch (GenICam::GenericException &e)
    {
        // Error handling.
        std::cerr << "An exception occurred in capture worker." << std::endl
                  << e.GetDescription() << std::endl;
    }
    QMetaObject::invokeMethod(this, "onCapture", Qt::QueuedConnection);

}

void BoostCaptureWorker::onSetCaptureMode(int captureMode)
{
    this->captureMode = captureMode;
}
