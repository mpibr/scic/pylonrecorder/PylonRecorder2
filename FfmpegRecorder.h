#ifdef WITH_FFMPEG

#ifndef FFMPEGRECORDE_H
#define FFMPEGRECORDE_H
/*
 * Based on https://gist.github.com/yohhoy/52b31522dbb751e5296e
 * Convert from OpenCV image and write movie with FFmpeg
 * Original version 2016 by yohhoy
 */
#include "Recorder.h"
#include <iostream>
#include <vector>
#include <QObject>
#include <QString>
#include <QDebug>
#include <QByteArray>
#include <QTime>

// FFmpeg
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/mathematics.h> //only required in older ffmpeg versions
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
#include <libavformat/avio.h>
#include <libavutil/imgutils.h> //for av_image_alloc only
#include <libavutil/opt.h>
}

// OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

class FfmpegRecorder : public Recorder
{
    Q_OBJECT
public:
    explicit FfmpegRecorder(QObject *parent = nullptr, int gpu = 0);
    QString ffmpeg_codec = "h264_nvenc";
//    QString ffmpeg_codec = "libx264";

private:
    AVFormatContext* outputFormatContext;
    AVCodecContext* codecContext;
    AVCodec* vcodec;
    AVStream* vstream;
    SwsContext* swsContext;
    AVFrame* frame;
    int64_t frame_pts = 0;

    bool wasClosed = false;

    long long nImagesToGrab = 0;
    long long nImagesGrabbed = 0;
    bool isRecording = false;
    int gpu;
    void closeFile();
    void printAVErrorMessage(int returnCode);
signals:

public slots:
    void onFrameGrabbed(const cv::Mat &);
    void onVideoOpen(QString fileName, double frameRate, cv::Size frameSize);
    void onVideoClose();
    void onSetNImagesToGrab(long long);


};

#endif // FFMPEGRECORDE_H
#endif
