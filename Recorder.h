#ifndef RECORDER_H
#define RECORDER_H

#include <QThread>
#include <QObject>
#include <QDebug>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

class Recorder : public QObject
{
    Q_OBJECT
public:
    explicit inline Recorder(QObject *parent = nullptr): QObject(parent){}

    int codec = cv::VideoWriter::fourcc('X','2','6','4');
    int colorConversion = cv::COLOR_BayerRG2RGB;
    int nChannels;
    double compression;

signals:
    void frameWritten(long long);
    void allWritten();
    void statusInfo(QString info);

public slots:
    virtual void onFrameGrabbed(const cv::Mat &) = 0;
    virtual void onVideoOpen(QString fileName, double frameRate, cv::Size frameSize) = 0;
    virtual void onVideoClose() = 0;
    virtual void onSetNImagesToGrab(long long) = 0;


};


#endif // RECORDER_H
