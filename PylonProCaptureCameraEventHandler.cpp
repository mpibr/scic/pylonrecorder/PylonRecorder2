#include "PylonProCaptureCameraEventHandler.h"

// Only very short processing tasks should be performed by this method. Otherwise, the event notification will block the
// processing of images.
void PylonProCaptureCameraEventHandler::OnCameraEvent( Pylon::CInstantCamera& camera, intptr_t userProvidedId, GenApi::INode* )
{
    using namespace Pylon;
    switch ( userProvidedId )
    {
    case captureFrameStartEvent:
        //std::cout << nImagesGrabbed << " / " <<nImagesToGrab << std::endl;
        if(nImagesGrabbed==0){
            std::cout << "First Burst"<< std::endl;
            emit(statusInfo("Camera was triggered"));
            emit(burstStartEvent());
        }
        nImagesGrabbed++;

        if((nImagesToGrab>0) | (nImagesGrabbed == std::numeric_limits <long long>::max())){
            if((nImagesGrabbed >= nImagesToGrab) | (nImagesGrabbed == std::numeric_limits <long long>::max())){
                emit(statusInfo("Captured " + QString::number(nImagesGrabbed) + " frames"));
                std::cout<<"Eventhandler received: " << nImagesGrabbed<< " frames" << std::endl;
                emit(burstStopEvent());
            }
        }
        break;

    case captureExposureEndEvent:
        break;

    case captureFrameStartOvertrigger:
        // The camera has been overtriggered.
        emit(statusInfo("WARNING: Overtriggered!"));
        std::cout<<"WARNING: Overtriggered!"<< std::endl;
        break;

    case captureFrameBurstStartOvertrigger:
        // The camera has been overtriggered.
        emit(statusInfo("WARNING: Overtriggered burst!"));
        std::cout<<"WARNING: Overtriggered burst!"<< std::endl;
        break;

    case captureEventOverrun:
        // The camera was unable to send all its events to the PC.
        // Events have been dropped by the camera.
        emit(statusInfo("WARNING: Events have been dropped!"));
        std::cout<<"WARNING: Events have been dropped!"<< std::endl;
        break;
    }
}

void PylonProCaptureCameraEventHandler::onResetCounter()
{
    nImagesGrabbed = 0;
    std::cout << "Eventhandler: Resetting counters" << std::endl;
}

void PylonProCaptureCameraEventHandler::onSetImagesToGrab(long long nImagesToGrab)
{
    this->nImagesToGrab = nImagesToGrab;
    nImagesGrabbed = 0;
    std::cout << "Eventhandler set: " << nImagesToGrab << " frames" << std::endl;
}

void PylonProCaptureCameraEventHandler::onSetCaptureMode(int captureMode)
{
    std::cout << "CaptureMode set: " << captureMode << std::endl;
    this->captureMode = captureMode;
}
