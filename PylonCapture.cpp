#include "PylonCapture.h"

PylonCapture::PylonCapture(QObject *parent, int cameraIdx, uint stopLine, bool isTriggered, QString configFile, bool usbTweaksEnabled)
    : Capture(parent, stopLine, isTriggered)
{
    try{
        // Get the transport layer factory.
        Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();

        Pylon::DeviceInfoList_t devices;
        tlFactory.EnumerateDevices(devices, false);
        qDebug()<< "PylonRecorder found: " << devices.size() << " cameras!";
        if(devices.size()==0){
            throw RUNTIME_EXCEPTION("No compatible Basler cameras found");
        }

        if(cameraIdx >= 0){
            if(size_t(cameraIdx) < devices.size()){
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                }
            }else{
                throw RUNTIME_EXCEPTION("Camera index > detected cameras");
            }

        }else{
            bool isDeviceCreated = false;
            for(cameraIdx = 0; size_t(cameraIdx) < devices.size(); cameraIdx++){
                //Try to access first available camera
                if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIdx)])){
                    pDevice = tlFactory.CreateDevice(devices[size_t(cameraIdx)]);
                    isDeviceCreated = true;
                    break;
                }
            }
            if(!isDeviceCreated)
                throw RUNTIME_EXCEPTION("Could not connect to Basler camera (camera in use?)");

        }

        camera = new Pylon::CInstantCamera();
        camera->Attach(pDevice);

        // currently this is used to detect "pro" cameras that don't seem to have this event and don't support
        // changing the trigger strategy while open. These cameras currently rely on PylonProCapture.
        if(!GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStartOvertrigger"))){
            isAvailable = false;
            return;
        }


        // Print the model name of the camera.
        modelName = QString(camera->GetDeviceInfo().GetModelName());

        // When using the grab loop thread provided by the Instant Camera object, an image event handler processing the grab
        // results must be created and registered.
        cImageEventGrabber = new CaptureImageEventHandler(this);
        cCameraEventHandler = new CaptureCameraEventHandler(this);

        connect(cImageEventGrabber, &CaptureImageEventHandler::imageEvent, this, &PylonCapture::sendFrame);
        connect(cImageEventGrabber, &CaptureImageEventHandler::startTimeStampEvent, this, &PylonCapture::startTimeStampEvent);
        connect(cImageEventGrabber, &CaptureImageEventHandler::stopTimeStampEvent, this, &PylonCapture::stopTimeStampEvent); //this signal is not emitted reliably (hence the cCameraEventHandler is also used for stopping)
        connect(cImageEventGrabber, &CaptureImageEventHandler::timeStampEvent, this, &PylonCapture::timeStampEvent);
        connect(cImageEventGrabber, &CaptureImageEventHandler::statusInfo, this, &PylonCapture::statusInfo);
        connect(cImageEventGrabber, &CaptureImageEventHandler::lineInputEvent, this, &PylonCapture::lineInputEvent);

        connect(cCameraEventHandler, SIGNAL(statusInfo(QString)), this, SIGNAL(statusInfo(QString)));

        // These events are forwarded to add a timeStamp
        connect(cCameraEventHandler, &CaptureCameraEventHandler::burstStartEvent, cImageEventGrabber, &CaptureImageEventHandler::onBurstStartEvent);
        connect(cCameraEventHandler, &CaptureCameraEventHandler::burstStopEvent, cImageEventGrabber, &CaptureImageEventHandler::onBurstStopEvent);
        connect(cCameraEventHandler, &CaptureCameraEventHandler::burstStopEvent, this, [this](){
                emit stopTimeStampEvent(0);
            });

        connect(this, &PylonCapture::setCaptureMode, cCameraEventHandler, &CaptureCameraEventHandler::onSetCaptureMode);
        connect(this, &PylonCapture::setImagesToCapture, cCameraEventHandler, &CaptureCameraEventHandler::onSetImagesToGrab);
        connect(this, &PylonCapture::resetCounter, cCameraEventHandler, &CaptureCameraEventHandler::onResetCounter);

        // Register events
        camera->RegisterImageEventHandler( cImageEventGrabber, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);

        if(camera->GetSfncVersion() >= Pylon::Sfnc_2_2_0){
            // FrameStart is not available on some cameras. Try to use ExposureEnd instead.
            frameEventName = "EventFrameStart";
            if(!GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName(frameEventName))){
                frameEventName = "EventExposureEnd";
            }
            camera->RegisterCameraEventHandler( cCameraEventHandler, frameEventName, captureFrameStartEvent, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_None);
        }else{
            qDebug() << "Sfnc version is < 2.2.0";
            frameEventName = "ExposureEndEventData";
            camera->RegisterCameraEventHandler( cCameraEventHandler, frameEventName, captureFrameStartEvent, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_None);
            // The handler is registered for both, the ExposureEndEventFrameID and the ExposureEndEventTimestamp
            // node. These nodes represent the data carried by the Exposure End event.
            // For each Exposure End event received, the handler will be called twice, once for the frame ID, and
            // once for the time stamp.
            //camera.RegisterCameraEventHandler( pHandler2, "ExposureEndEventFrameID", eMyExposureEndEvent, RegistrationMode_Append, Cleanup_None );
            //camera.RegisterCameraEventHandler( pHandler2, "ExposureEndEventTimestamp", eMyExposureEndEvent, RegistrationMode_Append, Cleanup_None );
            qDebug() << "Sfnc Version < 2.2.0, using event:" << frameEventName << "!";
        }



        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStartOvertrigger"))){
            camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameBurstStartOvertrigger"))){
            camera->RegisterCameraEventHandler( cCameraEventHandler, "EventFrameBurstStartOvertrigger", captureFrameStartOvertrigger, Pylon::RegistrationMode_Append, Pylon::Cleanup_None);
        }

        // Register the standard configuration event handler for enabling software triggering.
        // The software trigger configuration handler replaces the default configuration
        // as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
        if(camera->GetSfncVersion() >= Pylon::Sfnc_2_2_0){
            camera->RegisterConfiguration( new Pylon::CAcquireBurstSoftwareTriggerConfiguration, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);
        }

        // More configurations:
        //camera->RegisterConfiguration( new CAcquireContinuousConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        //camera->RegisterConfiguration( new CAcquireSingleFrameConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        //camera->RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);

        // Camera event processing must be activated first, the default is off.
        camera->GrabCameraEvents = true;


        camera->Open();
        if(!configFile.isEmpty()){
            QFileInfo pfsFileInfo(configFile);

            if(pfsFileInfo.exists()){
                Pylon::CFeaturePersistence::Load( configFile.toLatin1().data(), &camera->GetNodeMap(), true );
                emit statusInfo("Loaded configuration from " + configFile);
            }else{
                Pylon::CFeaturePersistence::Save( configFile.toLatin1().data(), &camera->GetNodeMap() );
            }
        }

        // Set Camera parameters
        if(!GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStart"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameStart");
        }else{
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("ExposureEnd");
        }
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameBurstStart"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStart");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameStartOvertrigger"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameStartOvertrigger");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("EventFrameBurstStartOvertrigger"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->FromString("FrameBurstStartOvertrigger");
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventNotification"))->FromString("On");
        }

        // Use fixed FrameRate
        //GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->SetValue(true);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Width"))->SetValue(width);
        //GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Height"))->SetValue(width);
        //GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(frameRate);

        // Get Camera parameters
        GenApi::CIntegerPtr width = camera->GetNodeMap().GetNode("Width");
        GenApi::CIntegerPtr height = camera->GetNodeMap().GetNode("Height");
        frameSize = cv::Size(int(width->GetValue()), int(height->GetValue()));
        if (!bool(GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateEnable"))->GetValue())){
            emit statusInfo("no constant fps set");
            qDebug()<< "No constant fps set";
        }
        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate")))){
            frameRate = uint(GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->GetValue());
        }else{
            frameRate = uint(GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateAbs"))->GetValue());
        }

        GenApi::CEnumerationPtr pixelFormat = camera->GetNodeMap().GetNode("PixelFormat");
        QString pixelFormatString = QString::fromStdString(std::string(pixelFormat->ToString())); //using string since enums vary between USB and GigE

        if ( pixelFormatString.compare("BayerRG8")==0 ){
            colorConversion = cv::COLOR_BayerRG2RGB;
            nChannels = 1;
            qDebug() << "PylonCapture: converting from BayerRG8";
        }else if ( pixelFormatString.compare("BayerBG8")==0 ){
            colorConversion = cv::COLOR_BayerBG2RGB;
            nChannels = 1;
            qDebug() << "PylonCapture: converting from BayerBG8";
        }else if ( pixelFormatString.compare("RGB8")==0 ){
            colorConversion = 0;//cv::COLOR_BGR2RGB;
            //color conversion is done in the CaptureImageEventHandler
            cImageEventGrabber->isConversionEnabled = true;
            nChannels = 3;
            qDebug() << "PylonCapture: converting from BGR";
        }else if ( pixelFormatString.compare("Mono8")==0 ){
            colorConversion = 0;
            nChannels = 1;
            qDebug() << "PylonCapture: No conversion - using Mono";
        }
        cImageEventGrabber->nChannels = nChannels;


        // Low level usb tweaks
        if(usbTweaksEnabled){
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxNumBuffer"))->SetValue(8); //16
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxBufferSize"))->SetValue(131072); //131072
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("MaxTransferSize"))->SetValue(16384); //65536 or 32768
            GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("NumMaxQueuedUrbs"))->SetValue(64); //64
            //GenApi::CIntegerPtr(camera->GetStreamGrabberNodeMap().GetNode("TransferLoopThreadPriority"))->SetValue(5);
        }

        // For each output line, a specific UserOutput parameter is available to set the line as "user settable":
        // UserOutput 1 is available for output line Line 2
        //
        // UserOutput 2 is available for GPIO line Line 3 if the line is configured for output
        //
        // UserOutput 3 is available for GPIO line Line 4 if the line is configured for output.
        //
        // Set output line Line 2 to user settable

        qDebug()<< "PylonCapture: Testing LineModes to configure user output.";

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line4"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line4"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput3"))){
                    if(outputString.empty()){
                        //Line 4 is set for output
                        outputString = "UserOutput3";
                    }
                }
            //If this Line is not used as stopLine & trigger not set, use as trigger
            }else if(stopLine != 4 && triggerLine == 0){
                triggerLine = 4;
            }
        }


        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->GetEntryByName("Line3"))){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSelector"))->FromString("Line3"); //Select Line
            if(QString(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineMode"))->ToString()).compare("Output") == 0){
                if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("LineSource"))->GetEntryByName("UserOutput2"))){
                    //Line 3 is set for output
                    outputString = "UserOutput2";
                }
            }else{
                if (stopLine != 3 && triggerLine == 0){ //If this Line is not used as stopLine, use as trigger
                    triggerLine = 3;
                }
            }
        }


        if (stopLine != 0){
            stopLineString = GenICam::gcstring(QString("Line").append(QString::number(stopLine)).toLatin1());
            cImageEventGrabber->stopLineString = stopLineString;
            qDebug() << "PylonCapture: External stop on" << stopLineString;
        }

        if (triggerLine != 0){
            triggerLineString = GenICam::gcstring(QString("Line").append(QString::number(triggerLine)).toLatin1());
            qDebug() << "PylonCapture: External trigger on" << triggerLineString;
        }

        qDebug() << "PylonCapture: User defined output on" << outputString;

        isAvailable = true;
    }
    catch (GenICam::GenericException &e)
    {
        isAvailable = false;
        Pylon::PylonTerminate();
        // Error handling.
        std::cerr << "An exception occurred." << std::endl
                  << e.GetDescription() << std::endl;
    }
}

void PylonCapture::setFrameRate(double frameRate)
{
    if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate")))){
        GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->SetValue(int(frameRate));
    }else if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateAbs")))){
        GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRateAbs"))->SetValue(int(frameRate));
    }
}

void PylonCapture::init(int captureMode, long long nImagesToCapture)
{
    // Start the grabbing using the grab loop thread, by setting the grabLoopType parameter
    // to GrabLoop_ProvidedByInstantCamera. The grab results are delivered to the image event handlers.
    // The GrabStrategy_OneByOne default grab strategy is used.
    camera->StopGrabbing();
    setTriggered(true);

    // This is required to to the implementation of the callback
    // one frame more needs to be acquired for completion
    if(nImagesToCapture>0){
        nImagesToCapture++;
    }

    emit setImagesToCapture(nImagesToCapture);
    emit setCaptureMode(captureMode);
    emit resetCounter();
    switch(captureMode){
    case CAPTURE_CONTINUOUS:
        camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized continuous grabbing";
        break;
    case CAPTURE_BURST:
        if(nImagesToCapture>0){
            camera->StartGrabbing(size_t(nImagesToCapture), Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }else{
            camera->StartGrabbing(Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        }
        qDebug()<<"PylonCapture: Initialized burst grabbing";
        break;
    case CAPTURE_FRAMEWISE:
        camera->StartGrabbing( Pylon::GrabStrategy_OneByOne, Pylon::GrabLoop_ProvidedByInstantCamera);
        qDebug()<<"PylonCapture: Initialized framewise grabbing";
        break;
    }

    emit statusInfo("Camera initialized");
}

void PylonCapture::stop()
{
    camera->StopGrabbing();
}

void PylonCapture::softwareTrigger()
{
    if(!isTriggered)
        camera->ExecuteSoftwareTrigger();
}

void PylonCapture::setExternalTrigger(bool hasExternalTrigger)
{
    if(triggerLine != 0){
        if(hasExternalTrigger){
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString(triggerLineString);
            qDebug() << "PylonCapture: Enabled trigger on" << triggerLineString;
        }else{
            GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerSource"))->FromString("Software");
        }
    }
}

void PylonCapture::setUserOutput(bool value)
{
    if(!outputString.empty()){
        qDebug()<< "PylonCapture: User output " << value;
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("UserOutputSelector"))->FromString(outputString);
        GenApi::CBooleanPtr(camera->GetNodeMap().GetNode("UserOutputValue"))->SetValue(value);
    }
}

void PylonCapture::onCameraCommand(std::string command, double data)
{
    if (command == "exposure") {
        GenApi::CFloatPtr(camera->GetNodeMap().GetNode("ExposureTime"))->SetValue(int(data));
    }
}

void PylonCapture::setTriggered(bool isTriggered)
{
    if(isTriggered)
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "On");
    else
        GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("TriggerMode"))->FromString( "Off");
}

PylonCapture::~PylonCapture()
{
    if(isAvailable){
        camera->StopGrabbing();
        camera->Close();
        camera->DeregisterImageEventHandler(cImageEventGrabber);

        camera->DeregisterCameraEventHandler(cCameraEventHandler, frameEventName);

        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameStartOvertrigger"))){
            camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameStartOvertrigger");
        }
        if(GenApi::IsAvailable(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("EventSelector"))->GetEntryByName("FrameBurstStartOvertrigger"))){
            camera->DeregisterCameraEventHandler(cCameraEventHandler, "EventFrameBurstStartOvertrigger");
        }

        if(pDevice->IsOpen()){
            pDevice->Close();
        }
        // Releases all pylon resources.
        Pylon::PylonTerminate();
    }

    qDebug()<<"Terminate";
}
