#include "PylonProCaptureImageEventHandler.h"

PylonProCaptureImageEventHandler::PylonProCaptureImageEventHandler(QObject* parent)
    : QObject(parent)
{
     // Specify the output pixel format. Only used when BGR conversion happens
     formatConverter.OutputPixelFormat = Pylon::PixelType_BGR8packed;
}

void PylonProCaptureImageEventHandler::OnImageGrabbed( Pylon::CInstantCamera& camera, const Pylon::CGrabResultPtr& ptrGrabResult)
{
    //Line 3 was set High. Emit lineStopEvent()
    if(!stopLineString.empty()){
        GenApi::CEnumerationPtr(camera.GetNodeMap().GetNode("LineSelector"))->FromString(stopLineString);
        bool isHigh = GenApi::CBooleanPtr(camera.GetNodeMap().GetNode("LineStatus"))->GetValue();

        if(!lineWasHigh & isHigh){
            emit(lineInputEvent(true)); //line toggled from low to high
            qDebug()<<"CaptureImageEventHandler: Line3 turned high";
            lineWasHigh = true;
        }else if(lineWasHigh & !isHigh){
            emit(lineInputEvent(false)); // line toggled from high to low
            qDebug()<<"CaptureImageEventHandler: Line3 turned low";
            lineWasHigh = false;
        }
    }

    // Image grabbed successfully?
    if (ptrGrabResult->GrabSucceeded()){

        quint64 timeStamp = ptrGrabResult->GetTimeStamp();

        switch(eventStatus){
        case EVENT_START:
            emit(startTimeStampEvent(timeStamp));
            eventStatus = EVENT_TIMESTAMP;
            break;
        case EVENT_STOP:
            emit(stopTimeStampEvent(timeStamp));
            eventStatus = EVENT_TIMESTAMP;
            break;
        case EVENT_TIMESTAMP:
            emit(timeStampEvent(timeStamp));
            break;
        }

        //Note that all images have to be cloned since internally they have the reference to ptrGrabResult
        switch (nChannels){
        // Create an OpenCV image from a pylon image.
        case 1:
            emit(imageEvent(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC1, ptrGrabResult->GetBuffer()).clone()));
            break;
        case 3:
            if(isConversionEnabled){ //RGB to BGR is much faster in Pylon
                formatConverter.Convert(pylonImage, ptrGrabResult);
                emit(imageEvent(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *) pylonImage.GetBuffer()).clone()));
            }else{
                //Currently not used
                emit(imageEvent(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, ptrGrabResult->GetBuffer()).clone()));
            }
            break;
        }

    }else{
        std::cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << std::endl;
    }
}

void PylonProCaptureImageEventHandler::onBurstStartEvent()
{
    eventStatus = EVENT_START;
}

void PylonProCaptureImageEventHandler::onBurstStopEvent()
{
    eventStatus = EVENT_STOP;
}
