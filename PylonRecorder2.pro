QT       += core gui network
RC_FILE = app.rc
RESOURCES = application.qrc
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PylonRecorder
DEFINES += APP_VERSION=\\\"0.60\\\"
DEFINES += USB_OPENCV
DEFINES += WITH_FFMPEG
TEMPLATE = app
#QMAKE_CXXFLAGS += -Wno-deprecated-declarations #the FFMPEG has plenty of those

mac{
    QMAKE_CXXFLAGS += -F/Library/Frameworks
    INCLUDEPATH += /usr/local/opt/opencv3/include
    LIBS += -L/usr/local/opt/opencv3/lib
    LIBS += -lopencv_core
    LIBS += -lopencv_highgui
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_video
    LIBS += -lopencv_videoio

    INCLUDEPATH += /Users/kretschmerf/pylonHeaders
    INCLUDEPATH +=  /Users/kretschmerf/pylonHeaders/pylon/GenICam

    LIBS += -F/Library/Frameworks/ -framework pylon
    LIBS += -L/Library/Frameworks/pylon.framework/Versions/A/Libraries
    LIBS += -lpylonbase-5.0.11
    LIBS += -lpylonutility
    LIBS += -lGenApi_gcc_v3_0_Basler_pylon_v5_0
    LIBS += -lGCBase_gcc_v3_0_Basler_pylon_v5_0

    INCLUDEPATH += /usr/local/Cellar/qwt/6.1.3_1/lib/qwt.framework/Headers
}

unix{
    #Qt 5.9 is default under Ubuntu 18
    #Qt 5.12 is default under Ubuntu 20
    #Qt 5.15 is default under Ubuntu 22

    CONFIG += link_pkgconfig


    # opencv 4.5 (Ubuntu 22), install libopencv-dev
    PKGCONFIG += opencv4

    # opencv 3.2 (Ubuntu 18)
    #PKGCONFIG += opencv

    # opencv 4 (local install)
    #DEPENDPATH += /usr/local/lib
    #INCLUDEPATH += /usr/local/include/opencv4
    #INCLUDEPATH += /usr/local/include/opencv4/opencv2

    LIBS += -lopencv_core
    LIBS += -lopencv_imgcodecs
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_video
    LIBS += -lopencv_videoio

    PYLON_ROOT = /opt/pylon

    isEmpty(PYLON_ROOT){
        #use from environment
        PYLON_ROOT = $$(PYLON_ROOT)
    }

    CPPFLAGS += $$system( $$PYLON_ROOT/bin/pylon-config --cflags) -DUSE_GIGE
    LIBS += $$system( $$PYLON_ROOT/bin/pylon-config --libs-rpath) $$system( $$PYLON_ROOT/bin/pylon-config --libs)

    LIBS += -lavformat -lavcodec -lavutil -lswscale
    QMAKE_CXXFLAGS += -Wno-unknown-pragmas $$system( $$PYLON_ROOT/bin/pylon-config --cflags)
}

win32 {
    #QT 5.12 is latest LTS available for open source license
    #QT 5.15 latest LTS

 DEFINES += NOMINMAX
    #OPENCV_PATH = C:/Users/kretschmerf/libraries/opencv411/build #
    OPENCV_PATH = D:/Developer/Library/opencv/411/build
    #OPENCV_VERSION = 411 #343
    OPENCV_VERSION = 411
    INCLUDEPATH += $${OPENCV_PATH}/include


    CONFIG(debug,debug|release) {
        LIBS += -L$${OPENCV_PATH}/x64/vc15/lib \
            -lopencv_world$${OPENCV_VERSION}d
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L$${OPENCV_PATH}/x64/vc14/lib \
            -lopencv_world$${OPENCV_VERSION}
    }

#For this version of Visual C++  Use this compiler version
#Visual C++ 4.x                  MSC_VER=1000
#Visual C++ 5                    MSC_VER=1100
#Visual C++ 6                    MSC_VER=1200
#Visual C++ .NET                 MSC_VER=1300
#Visual C++ .NET 2003            MSC_VER=1310
#Visual C++ 2005  (8.0)          MSC_VER=1400
#Visual C++ 2008  (9.0)          MSC_VER=1500
#Visual C++ 2010 (10.0)          MSC_VER=1600
#Visual C++ 2012 (11.0)          MSC_VER=1700
#Visual C++ 2013 (12.0)          MSC_VER=1800!!
#Visual C++ 2015 (14.0)          MSC_VER=1900
#Visual C++ 2017 (15.0)

    INCLUDEPATH += "C:/Program Files/Basler/pylon 5/Development/include"

    LIBS += "-LC:/Program Files/Basler/pylon 5/Development/lib/x64" \
        -lGenApi_MD_VC141_v3_1_Basler_pylon \
        -lGCBase_MD_VC141_v3_1_Basler_pylon \
        -lPylonBase_v6_1 \
        -lPylonUtility_v6_1

    INCLUDEPATH += "D:/Developer/Library/ffmpeg-20191101-53c21c2-win64/include"
    LIBS += "-LD:/Developer/Library/ffmpeg-20191101-53c21c2-win64/bin"
    LIBS += "-LD:/Developer/Library/ffmpeg-20191101-53c21c2-win64/lib"

    LIBS += -lavformat -lavcodec -lavutil -lswscale

}

CONFIG += c++11

SOURCES +=\
    BoostCapture.cpp \
    BoostCaptureWorker.cpp \
    CaptureCameraEventHandler.cpp \
    CaptureImageEventHandler.cpp \
        MainWindow.cpp \
    AboutDialog.cpp \
    PylonCapture.cpp \
    PylonProCapture.cpp \
    PylonProCaptureCameraEventHandler.cpp \
    PylonProCaptureImageEventHandler.cpp \
    TimeConversion.cpp \
    USBCapture.cpp \
    USBCaptureWorker.cpp \
    TrackerRecordingWorker.cpp \
    RamMonitor.cpp \
    CameraEngine.cpp \
    FfmpegRecorder.cpp \
    OpenCVRecorder.cpp \
    SocketInterface.cpp \
    main.cpp

HEADERS  += MainWindow.h \
    AboutDialog.h \
    BoostCapture.h \
    BoostCaptureWorker.h \
    CAcquireBurstSoftwareTriggerConfiguration.h \
    CaptureCameraEventHandler.h \
    CaptureImageEventHandler.h \
    PylonCapture.h \
    PylonProCapture.h \
    PylonProCaptureCameraEventHandler.h \
    PylonProCaptureImageEventHandler.h \
    TimeConversion.h \
    CaptureEvents.h \
    Capture.h \
    USBCapture.h \
    USBCaptureWorker.h \
    CommandLineOptions.h \
    TrackerInterface.h \
    TrackerWorkerInterface.h \
    TrackerRecordingWorker.h \
    RamMonitor.h \
    CameraEngine.h \
    GuiOptions.h \
    Recorder.h \
    FfmpegRecorder.h \
    OpenCVRecorder.h \
    BlockReader.h \
    BlockWriter.h \
    SocketInterface.h

FORMS    += MainWindow.ui \
    AboutDialog.ui

DISTFILES += \
    PylonRecorderModel.qmodel \
    app.rc

RESOURCES +=

OTHER_FILES += \
    README.md \
    mpi-brain-research.png \
    PylonRecorder.ico \
    app.rc \
    logo_scic.png


