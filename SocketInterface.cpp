#include "SocketInterface.h"

SocketInterface::SocketInterface(QObject* parent, QString hostname, unsigned int port)
    : QObject(parent), hostname(hostname), port(port)
{
    int socketType;
    if ( port == 0 ){
        socketType = SocketType::NONE;
    }else if( (hostname.isEmpty()) ){
        socketType = SocketType::SERVER;
    }else{
        socketType = SocketType::CLIENT;
    }

    switch(socketType){
    case(SocketType::NONE):
    {
        break;
    }
    case(SocketType::SERVER):
    {
        connect(&server, SIGNAL(newConnection()),
                this, SLOT(onClientConnected()));
        server.listen(QHostAddress::Any, port);
        break;
    }
    case(SocketType::CLIENT):
    {
        connectionTimer.setInterval(1000);
        clients.append( new QTcpSocket(this) );
        connect(&connectionTimer, &QTimer::timeout, this, &SocketInterface::onConnectServer);
        connect(clients.last(), &QTcpSocket::connected, this, &SocketInterface::onServerConnected);
        connect(clients.last(), &QTcpSocket::disconnected, this, &SocketInterface::onServerDisconnected);
        connect(clients.last(), SIGNAL(readyRead()), this, SLOT(onRead()));
        connectionTimer.start();
        break;
    }
    }

    signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(onClientDisconnected(int)));
}

SocketInterface::~SocketInterface()
{
    for(auto client:clients){
        client->disconnectFromHost();
    }
    server.close();
}

void SocketInterface::onClientConnected()
{
    clients.append(server.nextPendingConnection());
    connect(clients.last(), SIGNAL(readyRead()),
            this, SLOT(onRead()));

    connect(clients.last(), SIGNAL(disconnected()),
            this->signalMapper, SLOT(map()));

    signalMapper->setMapping(clients.last(), clients.size()-1);

    isConnected = true;
    qDebug()<<"Client connected";
    emit(statusInfo("Client connected"));

}

void SocketInterface::onClientDisconnected(int clientIdx)
{
    qDebug()<< "REMOVING" <<clientIdx;
    disconnect(clients[clientIdx], SIGNAL(readyRead()),
            this, SLOT(onRead()));
    signalMapper->removeMappings(clients[clientIdx]);
    clients.remove(clientIdx);
    if(clients.size()==0){
        isConnected = false;
        qDebug()<<"All clients disconnected";
    }else{
        qDebug()<<"Client disconnected";
    }
    emit(statusInfo("Client disconnected"));
}

void SocketInterface::onConnectServer()
{
    qDebug()<<"Client: Trying to connect";
    clients[0]->abort();
    clients[0]->connectToHost(hostname,
                                quint16(port));
    connectionAttempts++;
    if(connectionAttempts == maxNumberConnectionAttempts){
        connectionTimer.stop();
    }
}

void SocketInterface::onServerConnected()
{
    qDebug()<< "Client: connected";
    isConnected = true;
    connectionTimer.stop();
}

void SocketInterface::onServerDisconnected()
{
    isConnected = false;
    connectionTimer.start();
}

void SocketInterface::onRead()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    // Now read the block from the socket.
    quint64 blockType;
    BlockReader blockReader(socket);
    blockReader.stream() >> blockType;
    switch(blockType){

    case socket_protocol::SOCKET_TIMESTAMP:
    {
        qDebug()<<"Socket: Reading Timestamp";
        quint64 timeStamp;
        blockReader.stream() >> timeStamp;
        emit(socketTimeStamp(timeStamp));
        break;
    }

    case socket_protocol::SOCKET_START:
    {
        qDebug()<<"Socket: Reading Start";
        quint64 timeStamp;
        blockReader.stream() >> timeStamp;
        emit(socketStart(timeStamp));
        break;
    }

    case socket_protocol::SOCKET_STOP:
    {
        qDebug()<<"Socket: Reading Stop";
        quint64 timeStamp;
        blockReader.stream() >> timeStamp;
        emit(socketStop(timeStamp));
        break;
    }

    case socket_protocol::SOCKET_FILENAME:
    {
        qDebug()<<"Socket: Reading Filename";
        QByteArray latinFileName;
        blockReader.stream() >> latinFileName;
        emit(socketFileName(QString::fromLatin1(latinFileName)));
        break;
    }

    case socket_protocol::SOCKET_TRACKINGRESULT:
    {
        int trackingResultLength = int((blockReader.blockSize - 12)/sizeof(double));
        double val;
        std::vector<double> trackingResult;
        for(int i = 0; i < trackingResultLength ; ++i){
            blockReader.stream() >> val;
            trackingResult.push_back(val);
        }
        emit(socketTrackingResult(trackingResult));
        break;
    }

    case socket_protocol::SOCKET_QUIT:
    {
        qDebug()<<"Socket: Reading Quit";
        quint64 timeStamp;
        blockReader.stream() >> timeStamp;
        emit(socketQuit(timeStamp)); //TODO: Not working
        break;
    }
    }

}

void SocketInterface::onTimeStamp(quint64 timeStamp)
{
//    if(isConnected){
//        for (auto client:clients)
//            BlockWriter(client).stream()<< quint64(socket_protocol::SOCKET_TIMESTAMP) << timeStamp;
//        qDebug()<<"Socket: Writing TimeStamp";
//    }
}

void SocketInterface::onStartTimeStamp(quint64 timeStamp)
{
    if(isConnected){
        for (auto client:clients)
            BlockWriter(client).stream()<< quint64(socket_protocol::SOCKET_START) << timeStamp;
        qDebug()<<"Socket: Writing Start";
    }
}

void SocketInterface::onStopTimeStamp(quint64 timeStamp)
{
    if(isConnected){
        for (auto client:clients)
            BlockWriter(client).stream()<< quint64(socket_protocol::SOCKET_STOP) << timeStamp;
        qDebug()<<"Socket: Writing Stop";
    }
}

void SocketInterface::onFileName(QString fileName)
{
    if(isConnected){
        for (auto client:clients)
            BlockWriter(client).stream()<< quint64(socket_protocol::SOCKET_FILENAME) << fileName.toLatin1();
        qDebug()<<"Socket: Writing Filename";
    }
}

void SocketInterface::onStop()
{
    if(isConnected){
        for (auto client:clients)
            BlockWriter(client).stream()<< quint64(socket_protocol::SOCKET_STOP) << quint64(0);
        qDebug()<<"Socket: Writing Manual stop";
    }
}

void SocketInterface::onTrackingResult(std::vector<double> result)
{
    if(isConnected){
        for (auto client:clients){
            BlockWriter blockWriter(client);
            blockWriter.stream() << quint64(socket_protocol::SOCKET_TRACKINGRESULT);
            for(double r : result)
                blockWriter.stream() << r; //Serialize vector
        }
        qDebug()<<"Socket: Writing Tracking Result";
    }
}

void SocketInterface::onQuit()
{
    if(isConnected){
        for (auto client:clients){
            BlockWriter blockWriter(client);
            blockWriter.stream() << quint64(socket_protocol::SOCKET_QUIT)  << quint64(0);
        }
        qDebug()<<"Socket: Writing Tracking Result";
    }
}
