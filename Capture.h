//This is the main interface to the Pylon SDK

#ifndef CAPTURE_H
#define CAPTURE_H
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <QFileInfo>
#include <opencv2/core/core.hpp>

class Capture : public QObject
{
    Q_OBJECT

public:
    enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};
    QString modelName;
    QString friendlyName;
    QString serialNumber;
    cv::Size frameSize;
    double frameRate = 0;
    int colorConversion = 0;
    int nChannels;
    unsigned int stopLine;
    bool isTriggered;
    bool isAvailable = false;
    unsigned int triggerLine = 0;

    explicit inline Capture(QObject *parent = nullptr, uint stopLine = 0, bool isTriggered = false)
        : QObject(parent), stopLine(stopLine), isTriggered(isTriggered) {}

    virtual void init(int captureMode, long long nImagesToCapture) = 0;
    virtual void stop() = 0;
    virtual void softwareTrigger() = 0;
    virtual void setFrameRate(double) = 0;
    virtual void setTriggered(bool) = 0;
    virtual void setExternalTrigger(bool) = 0;

signals:
    void sendFrame(cv::Mat);
    void setImagesToCapture(long long);
    void setCaptureMode(int captureMode);
    void timeStampEvent(quint64);
    void startTimeStampEvent(quint64);
    void stopTimeStampEvent(quint64);
    void statusInfo(QString info);
    void resetCounter();
    void lineInputEvent(bool isHigh);

public slots:
    virtual void setUserOutput(bool) = 0;
    virtual void onCameraCommand(std::string, double data) = 0;

};

#endif // CAPTURE_H
