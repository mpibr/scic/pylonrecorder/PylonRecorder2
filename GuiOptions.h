#ifndef GUIOPTIONS_H
#define GUIOPTIONS_H

#include <QString>

enum CAPTUREMODES {CAPTURE_CONTINUOUS, CAPTURE_FRAMEWISE, CAPTURE_BURST};

struct GuiOptions{
    int captureMode = CAPTURE_CONTINUOUS;
    bool hasExternalTrigger = false;

    bool isRecording = false;
    bool isTrackingRecording = false;
    long long nImagesToGrab = 0;

    bool fileNameAutoModeEnabled = false;
    QString fileName;
    QString filePath = ".";

    bool trackingFileNameAutoModeEnabled = false;
    QString trackingFileName;
    QString trackingFilePath = ".";

};

#endif // GUIOPTIONS_H
