//This is the main interface to the Pylon SDK

#ifndef PYLONPROCAPTURE_H
#define PYLONPROCAPTURE_H
#include <QWidget>
#include <QDebug>
#include <QThread>
#include <QFileInfo>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <pylon/PylonIncludes.h>
#include <pylon/InstantCamera.h>
#include "Capture.h"
#include "PylonProCaptureImageEventHandler.h"
#include "PylonProCaptureCameraEventHandler.h"

class PylonProCapture : public Capture
{
    Q_OBJECT

public:
    explicit PylonProCapture(QObject *parent = nullptr, int cameraIdx = 0, uint stopLine = 0, bool isTriggered = false, QString configFile = "", bool usbTweaksEnabled = false);
    ~PylonProCapture();
    virtual void init(int captureMode, long long nImagesToCapture);
    virtual void stop();
    virtual void softwareTrigger();
    virtual void setFrameRate(double); //Note: Pylon SDK takes int as argument
    virtual void setTriggered(bool);
    virtual void setExternalTrigger(bool);
    virtual void setUserOutput(bool);
    virtual void onCameraCommand(std::string, double);

private:
    // Automatically call Pylonlnitialize and PylonTerminate to ensure the pylon runtime system
    // is initialized during the lifetime of this object.
    Pylon::PylonAutoInitTerm autoInitTerm;

    // Pylon devices
    Pylon::IPylonDevice *pDevice = nullptr;
    Pylon::CInstantCamera *camera = nullptr;

    PylonProCaptureImageEventHandler* cImageEventGrabber;
    PylonProCaptureCameraEventHandler* cCameraEventHandler;

    GenICam::gcstring stopLineString;
    GenICam::gcstring outputString;
    GenICam::gcstring triggerLineString;

    bool hasExternalTrigger;
    int captureMode;
    Pylon::String_t frameEventName;

};

#endif // PYLONPROCAPTURE_H
