// Definition for a Tracking plugin

#ifndef TRACKERINTERFACE_H
#define TRACKERINTERFACE_H
#include <QtPlugin>
#include <QWidget>
#include <QLayout>
#include <QGraphicsScene>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

class TrackerInterface
{
public:
    virtual QObject *worker() = 0;
    virtual ~TrackerInterface() = 0;

    // Called after plugin was loaded or the resolution changes
    virtual void initializeUI(QLayout*, QGraphicsScene*, QSize) {} //guiTargetLayout, QcameraScene, cameraResolution
    inline bool getIsLogFileRecording(){
        return isLogFileRecording;
    }
protected:
    bool isLogFileRecording = false;

};

#define TrackerInterface_iid "org.PylonRecorder.TrackerInterface"

Q_DECLARE_INTERFACE(TrackerInterface, TrackerInterface_iid);


#endif // TRACKERINTERFACE_H
