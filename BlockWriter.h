#ifndef BLOCKWRITER_H
#define BLOCKWRITER_H
#include <QObject>
#include <QBuffer>
#include <QDataStream>

//https://stackoverflow.com/questions/19681564/read-from-qtcpsocket-using-qdatastream#19682690
class BlockWriter
{
public:
    BlockWriter(QIODevice *io)
    {
        buffer.open(QIODevice::WriteOnly);
        this->io = io;
        _stream.setVersion(QDataStream::Qt_4_8);
        _stream.setByteOrder(QDataStream::LittleEndian);
        _stream.setDevice(&buffer);

        // Placeholder for the size. We will get the value
        // at the end.
        _stream << quint64(0);
    }

    ~BlockWriter()
    {
        // Write the real size.
        _stream.device()->seek(0);
        _stream << quint64(buffer.size());

        // Flush to the device.
        io->write(buffer.buffer());
    }

    QDataStream &stream()
    {
        return _stream;
    }

private:
    QBuffer buffer;
    QDataStream _stream;
    QIODevice *io;
};
#endif // BLOCKWRITER_H
